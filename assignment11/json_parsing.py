"""This program determines current UTC date and time, and based on it
downloads monthly statistics (JSON data) for 1000 most visited
Wikimedia pages for the last 12 months.
    It adds each article and views count to a dictionary of key-value
pairs, summing the corresponding views counts for pages visited during
multiple months. It then displays the top 1000 articles in descending
order sorted  by views.
    Also, it finds top 100 learning projects, sorts (by views) and
displays them.

Input:
    none

Output:
    pages (top 1000, sorted in desc order based on the views)
    projects (top 100, sorted in desc order based on the views)

Example:
    TOP 1000 visited Wikimedia pages of last 12 months and their total views:

    Main_Page: 2,394,788.0
    Special:Search: 1,600,897.0
    Thesaurus_of_English_Words_and_Phrases: 1,146,564.0
    Special:CreateAccount: 936,126.0
    Dictionary_of_spoken_Spanish: 395,851.0
    The_Grammar_of_English_Grammars/Part_III: 224,270.0
    The_Grammar_of_English_Grammars/Part_II: 216,290.0
    ...

    TOP 100 visited Wikiversity projects of last 12 months and their
    total views:

    Main_Page: 2,394,788.0
    Thesaurus_of_English_Words_and_Phrases: 1,146,564.0
    Constitution_of_India: 640,612.0
    The_Grammar_of_English_Grammars: 451,659.0
    Dictionary_of_spoken_Spanish: 395,851.0
    The_Pearl: 374,489.0
    ...

References:
    * get relative timedelta (month) by importing relativedelta module:
        https://stackoverflow.com/questions/546321/how-do-i-calculate-
        the-date-six-months-from-the-current-date-using-the-datetime
    * PEP 378 - Format Specifier for Thousands Separator:
        https://www.python.org/dev/peps/pep-0378/
    * use of load(), loads(), dump(), dumps() methods:
        https://www.youtube.com/watch?v=9N6a-VLBa2I
"""
import sys
import datetime
import pytz
from dateutil.relativedelta import relativedelta
from urllib.request import urlopen
import json
from operator import itemgetter
import re


def get_utc_date():
    """Determines current UTC date and time.

    Args: none

    Returns: datetime_utc_now(datetime.datetime)
    """
    datetime_utc_now = datetime.datetime.now(tz=pytz.UTC)
    return datetime_utc_now


def determine_time_delta(datetime_utc_now, month):
    """Goes N months back in time by using relative delta.

    Args:
        datetime_utc_now(datetime.datetime)
        month(int; delta)

    Returns: previous_date(datetime.datetime)
    """
    previous_date = datetime_utc_now - relativedelta(months=month)
    return previous_date


def request_data(datetime_utc_now):
    """Requests monthly JSON data for the last 12 month. Current month
    is not being included because it's not over yet. It uses current
    UTF date and time as a starpoint.
    Calls determine_time_delta() function. Calls
    add_articles_to_dictionary() function.

    Args: datetime_utc_now(datetime.datetime)

    Returns: articles_dictionary
    """
    MONTHS_PER_YEAR = 12
    articles_dictionary = {}

    for month in range(1, MONTHS_PER_YEAR + 1):
        previous_date = determine_time_delta(datetime_utc_now, month)

        if previous_date.month >= 1 and previous_date.month <= 9:
            data_url = (f"https://wikimedia.org/api/rest_v1/metrics/" +
                        f"pageviews/top/en.wikisource/all-access/" +
                        f"{previous_date.year}/0{previous_date.month}/" +
                        f"all-days")
        else:
            data_url = (f"https://wikimedia.org/api/rest_v1/metrics/" +
                        f"pageviews/top/en.wikisource/all-access/" +
                        f"{previous_date.year}/{previous_date.month}/"
                        f"all-days")

        with urlopen(data_url) as articles:
            top_1000 = articles.read()
            top_1000 = json.loads(top_1000)
            articles_dictionary = add_articles_to_dictionary(
                                  articles_dictionary, top_1000)
    return articles_dictionary


def add_articles_to_dictionary(articles_dictionary, top_1000):
    """Adds articles and corresponding views to the dictionary if no
    match is found. If match is found, adds current views to the
    previous views, and updates the info in the dictionary.

    Args:
        articles_dictionary(empty dict)
        top_1000(json data in dict form)

    Returns: none
    """
    for single_article in top_1000["items"][0]["articles"]:
        if articles_dictionary.get(single_article["article"]) is None:
            articles_dictionary[single_article["article"]
                                ] = single_article["views"]
        else:
            articles_dictionary[single_article["article"]
                                ] += int(single_article["views"])
    return articles_dictionary


def create_projects_with_subpages(articles_dictionary):
    """Creates a dictionary of Wikimedia projects that HAVE subpages.

    Args: articles_dictionary(dict)

    Returns: subpage_projects(dict)
    """
    subpage_pattern = re.compile(r'(\w+)\/\w+')
    subpage_projects = {}

    for key in articles_dictionary:
        matches = subpage_pattern.finditer(key)
        for match in matches:
            if subpage_projects.get(match.group(1)) is None:
                subpage_projects[match.group(1)] = articles_dictionary[key]
            else:
                subpage_projects[match.group(1)] += articles_dictionary[key]
    return subpage_projects


def create_projects_no_subpages(articles_dictionary):
    """Creates a dictionary of Wikimedia projects that DON'T have subpages.

    Args: articles_dictionary(dict)

    Returns: no_subpage_projects(dict)
    """
    no_subpage_pattern = re.compile(r'\'(\w+)\'')
    no_subpage_projects = {}
    for key in articles_dictionary:
        new_key = "'" + key + "'"
        matches = no_subpage_pattern.finditer(new_key)
        for match in matches:
            if no_subpage_projects.get(match.group(1)) is None:
                no_subpage_projects[match.group(1)] = articles_dictionary[key]
            else:
                no_subpage_projects[match.group(1)] += articles_dictionary[key]

    return no_subpage_projects


def merge_projects(subpage_projects, no_subpage_projects):
    """Merges two dictionaries and adds values of common keys.

    Args:
        subpage_projects(dict)
        no_subpage_projects(dict)

    Returns:
        projects(dict)
    """
    projects = {**subpage_projects, **no_subpage_projects}
    for key in projects:
        if key in subpage_projects and key in no_subpage_projects:
            projects[key] = subpage_projects[key] + no_subpage_projects[key]

    return projects


def sort_articles(articles, number_of_articles, label):
    """Sorts wikimedia pages or projects in descending order based
    on the number of visits. Calls display_results() function.

    Args:
        articles_dictionary (dict) OR projects (dict)
        AND
        number_of_articles(int; number of records to be displayed)
        AND
        label(literal string)

    Returns: none
    """
    try:
        if type(articles) is not dict:
            raise AttributeError

        articles_list = sorted(articles.items(), key=itemgetter(1),
                               reverse=True)

        sorted_articles = {}
        record_count = 0

        for key, value in articles_list:
            sorted_articles[key] = value
            record_count += 1
            if record_count > number_of_articles:
                break

        display_results(sorted_articles, number_of_articles, label)

    except AttributeError:
        print("AttributeError: dictionary not of class type dict.")
        print("Received '%s' of type %s." % (articles,
              type(articles)))


def display_results(sorted_articles, number_of_articles, label):
    """Displays top sorted pages/projects.

    Args:
        sorted_articles(dict)
        number_of_articles(int; number of records to be displayed)
        label(literal string)

    Returns: none
    """
    print(f"\n\nTOP {number_of_articles} visited {label} of last 12 months" +
          f" and their total views:\n")
    for key in sorted_articles:
        print(key + ":", format(sorted_articles[key], "08,.1f"))


def main():
    """ Runs the main program logic. """
    try:
        datetime_utc_now = get_utc_date()
        articles_dictionary = request_data(datetime_utc_now)
        sort_articles(articles_dictionary, 1000, "Wikimedia pages")
        subpage_projects = create_projects_with_subpages(articles_dictionary)
        no_subpage_projects = create_projects_no_subpages(articles_dictionary)
        projects = merge_projects(subpage_projects, no_subpage_projects)
        sort_articles(projects, 100, "Wikimedia projects")

    except:
        print("Unexpected error.")
        print(f"Error: {sys.exc_info()[1]}")
        print(f"File: {sys.exc_info()[2].tb_frame.f_code.co_filename}")
        print(f"Line: {sys.exc_info()[2].tb_lineno}")


if __name__ == "__main__":
    main()
