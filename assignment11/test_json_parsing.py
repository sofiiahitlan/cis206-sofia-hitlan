"""This file tests JSON parsing program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    x passed in 0.xx seconds
"""

import pytest
import json_parsing
import json
from operator import itemgetter
import csv

utc_date = json_parsing.get_utc_date()


def test_get_utc_date_returns_correct_year_and_month():
    assert utc_date.year == 2020
    assert utc_date.month == 3


def test_determine_time_delta_returns_correct_previous_month():
    previous_date = json_parsing.determine_time_delta(utc_date, 10)
    assert previous_date.year == 2019
    assert previous_date.month == 5


def test_request_data_returns_correct_dictionary():
    file_path = 'articles_dict_for_testing.txt'

    with open(file_path, 'r', encoding='utf8') as file:
        data = file.read()

    json_data = json.loads(data)
    json_data = sorted(json_data.items(), key=itemgetter(1), reverse=True)

    data_to_test = json_parsing.request_data(utc_date)
    data_to_test = sorted(data_to_test.items(), key=itemgetter(1),
                          reverse=True)

    assert json_data == data_to_test


def test_add_articles_to_dictionary_returns_correct_dictionary():
    dictionary = {}
    test_data = {"items": [{"project": "en.wikisource", "access":
                            "all-access", "year": "2019", "month": "10",
                            "day": "all-days",
                            "articles": [{"article": "Main_Page",
                                          "views": 248026, "rank": 1},
                                         {"article": "Special:Search",
                                          "views": 147701, "rank": 2}]}]}
    expected_result = {"Main_Page": 248026, "Special:Search": 147701}
    assert json_parsing.add_articles_to_dictionary(
        dictionary, test_data) == expected_result


def test_create_projects_with_subpages_returns_correct_data_if_no_match():
    test_projects = {'The_Wonderful_Wizard_of_Oz/Subpage': 963070,
                     'The_Church_Hymnary': 496006}
    expected_result = {'The_Wonderful_Wizard_of_Oz': 963070}
    assert json_parsing.create_projects_with_subpages(
        test_projects) == expected_result


def test_create_projects_with_subpages_returns_correct_data_if_match():
    test_projects = {'The_Wonderful_Wizard_of_Oz/Subpage One': 25000,
                     'The_Wonderful_Wizard_of_Oz/Subpage Two': 10000,
                     'The_Church_Hymnary': 496006}
    expected_result = {'The_Wonderful_Wizard_of_Oz': 35000}
    assert json_parsing.create_projects_with_subpages(
        test_projects) == expected_result


def test_create_projects_no_subpages_returns_correct_data_if_no_match():
    test_projects = {'The_Wonderful_Wizard_of_Oz/Subpage': 963070,
                     'The_Church_Hymnary': 496006}
    expected_result = {'The_Church_Hymnary': 496006}
    assert json_parsing.create_projects_no_subpages(
        test_projects) == expected_result


def test_merge_projects_joins_projects_correctly_if_no_match():
    subpages = {'The_Wonderful_Wizard_of_Oz': 963070}
    no_subpages = {'The_Church_Hymnary': 496006}
    expected_result = {'The_Wonderful_Wizard_of_Oz': 963070,
                       'The_Church_Hymnary': 496006}
    assert json_parsing.merge_projects(
        subpages, no_subpages) == expected_result


def test_merge_projects_joins_projects_correctly_if_match():
    subpages = {'The_Wonderful_Wizard_of_Oz': 963070,
                'The_Church_Hymnary': 100000}
    no_subpages = {'The_Church_Hymnary': 496006}
    expected_result = {'The_Wonderful_Wizard_of_Oz': 963070,
                       'The_Church_Hymnary': 596006}
    assert json_parsing.merge_projects(
        subpages, no_subpages) == expected_result


def test_sort_articles_sorts_correctly_and_calls_display_function_that_outputs_correctly(capsys):
    test_data = {'The_Wonderful_Wizard_of_Oz': 963070,
                 'Casey_at_the_Bat': 10000,
                 'The_Church_Hymnary': 496006}
    expected_output = ("\n\nTOP 3 visited test articles of last 12 months " +
                       "and their total views:\n\nThe_Wonderful_Wizard_of_" +
                       "Oz: 963,070.0\nThe_Church_Hymnary: 496,006.0\n" +
                       "Casey_at_the_Bat: 10,000.0\n")
    json_parsing.sort_articles(test_data, 3, "test articles")
    captured = capsys.readouterr()
    assert captured.out == expected_output


def test_sort_articles_prints_error_message_if_argument_is_not_dictionary(capsys):
    json_parsing.sort_articles(["this is list"], 5, "test label")
    expected_error_message = ("AttributeError: dictionary not of class type" +
                              " dict.\nReceived '['this is list']' of type "
                              "<class 'list'>.\n")

    captured = capsys.readouterr()
    assert captured.out == expected_error_message


def test_display_results_displays_output():
    sorted_articles = {'The_Wonderful_Wizard_of_Oz': 963070,
                       'Casey_at_the_Bat': 496006,
                       'The_Church_Hymnary': 496006}
    json_parsing.display_results(
        sorted_articles, 3, "articles") == ("\n\nTOP 3 visited articles of " +
                                            "last 12 months and their total" +
                                            " views:\nThe_Wonderful_Wizard" +
                                            "_of_Oz: 963070\nCasey_at_the_Bat:" +
                                            " 496006\nThe_Church_Hymnary: " +
                                            "496006")
