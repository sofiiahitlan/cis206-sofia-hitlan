"""This file tests the Northwind customers sort and search program
using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    6 passed in 0.xx seconds

"""
import pytest
import tuples

northwind_customers = 'northwind_customers.txt'
# northwind_customers = (r'C:\Users\coni4\Desktop\College\c'
#                        r'is206 Applied Programming'
#                        r'\cis206-sofia-hitlan\assignment8\n'
#                        r'orthwind_customers.txt')

records = [
            ["CustomerID", "CompanyName", "ContactName"],
            ["ANTON", "Antonio Moreno Taquería", "Antonio Moreno"],
            ["CHOPS", "Chop-suey Chinese", "Yang Wang"]
          ]

def test_get_records_raises_FileNotFoundError_if_file_not_found():
    with pytest.raises(FileNotFoundError):
        tuples.get_records('non_existing_file.txt')


def test_get_records_returns_array():
    assert type(tuples.get_records(northwind_customers)) is list


def test_get_choice_returns_valid_input():
    input_values = ["one"]

    def input(prompt=None):
        return input_values.pop()

    tuples.input = input
    assert tuples.get_choice() == "one"


def test_get_choice_returns_error_message_on_valid_input():
    input_values = ["5", "5"]

    def input(prompt=None):
        return input_values.pop()

    tuples.input = input
    assert tuples.get_choice() == "5"


# def test_get_choice_empty_string_returns_none():
#     input_values = [""]

#     def input(prompt=None):
#         return input_values.pop()

#     tuples.input = input
#     assert tuples.get_choice() == None


# def test_process_choice_assigns_correct_key():

def test_sort_customers_returns_correct_results(capsys):
    tuples.sort_customers(records, 1, [1, 2])
    captured = capsys.readouterr()
    assert captured.out == ("Antonio Moreno Taquería\tAntonio Moreno\t\nChop" +
                            "-suey Chinese\tYang Wang\t\nCompanyName\t" +
                            "ContactName\t\n")


def test_search_customers_returns_correct_results_if_match_is_found(capsys):
    input_values = ["Antonio"]

    def input(prompt=None):
        return input_values.pop()

    tuples.input = input

    tuples.search_customers(records, "company name", 1)
    captured = capsys.readouterr()

    assert captured.out == ("Enter company name you want so search for." +
                            "\nCustomerID: ANTON, CompanyName: Antonio " +
                            "Moreno Taquería, ContactName: Antonio " +
                            "Moreno, \n\n")


def test_search_customers_prints_message_if_no_match_found(capsys):
    input_values = ["zone"]

    def input(prompt=None):
        return input_values.pop()

    tuples.input = input

    tuples.search_customers(records, "company name", 1)
    captured = capsys.readouterr()

    assert captured.out == ("Enter company name you want so search for.\nNo" +
                            " match found.\n")
