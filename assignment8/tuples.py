import os


def get_records(northwind_customers):
    """Reads the file, strips each line, makes each line a list,
    seperates each list with a comma.

    Args:
        northwind_customers (str): path to the file

    Returns:
        northwind_customers

    Raises:
        ValueError: If no path to file with customers was provided
    """
    try:
        with open(northwind_customers, "r") as file:
            records = []
            for line in file:
                line = line.strip()
                line = line.split(",")
                for index in range(len(line)):
                    line[index] = line[index].strip("\"")
                records.append(line)
                # print(records)
            return records
    except FileNotFoundError:
        print("File was not found.")
        os._exit(1)


def get_choice():
    while True:
        try:
            print("Choose one of the following options:")
            print("Press 1 to sort customers by company name.")
            print("Press 2 to sort customers by contact name.")
            print("Press 3 to search for company by full or partial name.")
            print("Press 4 to search for contact person by full or partial" +
                  "name.")

            choice = input()
            if choice == "1" or choice == "one" or choice == "One" or choice == "2" or choice == "two" or choice == "Two" or choice == "3" or choice == "three" or choice == "Three" or choice == "4" or choice == "four" or choice == "Four" and choice != "":
                return choice
            else:
                print("Invalid input. Press any key to correct your input " +
                      "or enter \"exit\" to quit now.")
                exit_condition = input()
                if exit_condition == "exit" or exit_condition == "Exit":
                    os._exit(1)
                return choice
        except ValueError:
            if choice == "":
                return None
            else:
                print("Invalid input.")
                print("Your options are: \"1\", \"2\", \"3\", \"4\"")
                print("Press any key to correct your input or enter" +
                      "\"exit\" to quit now.")

                exit_condition = input()
                if exit_condition == "exit" or exit_condition == "Exit":
                    os._exit(1)


def process_choice(choice, records):
    if choice == "1" or choice == "one" or choice == "One":
        key = 1
        sort_customers(records, key, [1, 2, 9])
    elif choice == "2" or choice == "two" or choice == "Two":
        key = 2
        sort_customers(records, key, [2, 1, 9])
    elif choice == "3" or choice == "three" or choice == "Three":
        key = 1
        search_customers(records, "company name", key)
    elif choice == "4" or choice == 4 or choice == "four" or choice == "Four":
        key = 2
        search_customers(records, "contact person name", key)
    elif choice == "exit" or choice == "Exit":
        os._exit(1)
    else:
        print("Invalid choice.")


def sort_customers(records, key, keys):
    records = sorted(records, key=lambda record: record[key])

    for record in records:
        for key in keys:
            print(record[key], end="\t")
        print()


def search_customers(records, name, key):
    print(f"Enter {name} you want so search for.")
    search_query = input()
    try:
        # print("Results:\n")
        is_match = False
        for element in records:
            i = 0
            if search_query.lower() in element[key].lower():
                is_match = True
                for item in element:
                    print(records[0][i], end=": ")
                    print(item, end=", ")
                    i += 1
                print("\n")
        if is_match is False:
            print("No match found.")
    except ValueError:
        print("Error. Exiting.")
        os._exit(1)


def main():
    northwind_customers = 'northwind_customers.txt'
    northwind_customers = (r'C:\Users\coni4\Desktop\College\c'
                           r'is206 Applied Programming'
                           r'\cis206-sofia-hitlan\assignment9\n'
                           r'orthwind_customers.txt')
    records = get_records(northwind_customers)
    choice = get_choice()
    process_choice(choice, records)


if __name__ == "__main__":
    main()
