# Sofia Hitlan

## Assignment 1

I'm taking this course to enhance my programming skills. I think this class can save me from doing coding bootcamps in the future.
I decided to go with Python because I used it in the CIS106 class and enjoyed it a lot. The second reason is that Python is one of the fastest-growing programming languages. Now is a good time to learn it.

## Assignment 2
This week I took a little time to reprogram my thinking from JS- like to Python-like. I reminded myself what Python function syntax looks like. I rediscovered the main function and absence of curly braces and semicolons. The conclusion I made is that there should be no breaks in between learning different programming languages. I'll try not to make this mistake again.

## Assignment 3
This week I learned that functions from sys and os modules are used to handle errors and exceptions. I became familiar with improved string formatting syntax called f-strings. Error handling made my code about 100 lines longer, and it's way harder to read it/navigate through it now. I hope to get more practice using error handling soon so that I can use it more confidently.

## Assignment 4
This week I learned that do-while loops are represented in Python by while True statement. The latter loop executes indefinitely. I'm yet to learn how to break down from it without confusing the user.

## Assignment 5
This week I learned the importance of testing and the fact that it's a crucial part of the software development process. The testing principle that stood out to me was the one saying that exhaustive testing is impossible. Just like in the security field, in programming, a risk assessment will help to determine the amount of coverage needed for a particular program. I've become familiar with the concept of test-driven programming. My short term goal is to get used to reading coverage reports and understanding what tests I can use to improve them.

## Assignment 6
This week I learned that string characters could be accessed using indexes just like list elements are. Strings can be navigated and sliced using either positive or negative indexes. Slicing using [start:stop:step] syntax is a new concept for me. Next time I'll be working with lists and strings, I'll think about how I can apply this concept.

## Assignment 7
This week I learned that in Python "with" keyword is used both for opening and closing files. Closing will happen automatically. A thing to remember is that appending to or opening a non-existing file will create that file. I focused on using regex patterns to determine password vulnerabilities. The syntax was challenging to use at first, but I feel more confident now about using it for my future assignments and projects.

## Assignment 8
This week I learned that tuples are immutable, and lists are not. It seems to me that lists are not always the best pick because they are passed by reference. It means that manipulation with lists can deliver unexpected results. I'm yet to find out if it can create problems, and if yes, how bad those problems are. I haven't used tuples for this assignment and hope to get more practice using them to understand better when to choose tuples over lists.

## Assignment 9
This week I learned that the dictionary holds non-ordered value pairs. I've found out the difference between adding values to the dictionary and the list. 
I saw in action what it means for dictionaries to be passed by reference. I kept overwriting my current dictionary every time the loop ran, so the list that I created for holding customer records has only contained multiple copies of the same record.
I'll keep using lists whnever it makes sense to subscript them using indexes, and will use disctionaries when indecies work better.
I'll keep using lists whenever it makes sense to subscript the data using indexes. I'll use dictionaries when indices are a better choice for the situation.

## Assignment 10
This week I learned how to write basic regex patterns. It gives me a chance to read and better understand patterns wrote by someone else. That comes handy because a lot of patterns are now available online. 
I now know that decompressing and decoding files are different concepts. I found out that python.org provides clear explanations and has code examples that can be implemented into the program I'm working on. I'll keep coming back to the Python website when I need some clarifications. Stackoverflow doesn't always have up-to-date answers.

## Assignment 11
This week I learned that JSON data could be used not only in JavaScript. Python's JSON library has methods that convert the JSON string to the Python object, and vice versa. While load(), loads(), dump(), and dumps ()methods seem pretty confusing, I'll keep using them by referring to examples available at Python.org.
This week I got once step closer to understanding how pytest testing works. I'm planning to go over pytest documentation in the short term to learn more about available pytest fixtures.

## Assignment 12
This week I learned how to incorporate SQLite queries into Python code. That was less challenging than I expected. I surely need more practice with writing SQL commands myself.  Outputting records in appropriately sized columns was something that blew my mind. I'm looking forward to modifying databases next week. I hope we will cover the best practices of backing up databases. 
Closer to the end of the semester, it becomes more clear to me what to expect from real-life programming. Thank you for building your courses this way.

## Assignment 13
This week showed me what databases are really about. I didn't use quotes carefully, and it was hard to catch was I was doing wrong. An SQL command would execute successfully one time but would fail the other time. Debugging with Thonny wasn't easy because of long SQL commands that got truncated. It made it impossible to find an error. I think it's time to learn how to debug using VSC.
Creating an all-inclusive testing program was complicated. If a database gets modified, my test program will fail. It's something I need to keep in mind to avoid hardcoding in the future.
Making dozens of database copies and renaming them back and forth confused me a lot. I'll need to be more organized not to lose data one day.

## Assignment 14
This week I got familiar with Python classes syntax. I learned that PEP8 rules are somewhat different for classes. Double empty lines are not required to separate methods. I have a hard time understanding how modules created by different people are supposed to work together, but hopefully, that will make more sense in a few weeks. 