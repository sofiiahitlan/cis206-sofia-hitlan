"""This file tests the BMI calculator program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    8 passed in 0.xx seconds

References:
    * https://en.wikiversity.org/wiki/Applied_Programming/Testing/Python3
    
"""

import pytest
import activity1


# WELCOME FUNCTION
def test_display_welcome(capsys):
    activity1.display_welcome()
    captured = capsys.readouterr()
    assert captured.out == "Welcome to the Adult Body Mass Index Calculator! \n\n"


# INPUT TESTING - GET POUNDS
def test_get_pounds_returns_valid_input():
    input_values = ['100']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_pounds() == 100


def test_get_pounds_ignores_zero():
    input_values = ['0', '234']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_pounds() == 234


def test_get_pounds_ignores_below_zero():
    input_values = ['-127', '10']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_pounds() == 10


def test_get_pounds_ignores_string_value():
    input_values = ['error', '150']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_pounds() == 150


def test_get_pounds_empty_string_returns_none():
    input_values = ['']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_pounds() == None


# INPUT TESTING - GET HEIGHT FEET
def test_get_height_feet_returns_valid_input():
    input_values = ['5']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_feet() == 5


def test_get_height_feet_ignores_zero():
    input_values = ['0', '7']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_feet() == 7


def test_get_height_feet_ignores_below_zero():
    input_values = ['-4', '6']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_feet() == 6


def test_get_height_feet_ignores_string_value():
    input_values = ['error', '5']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_feet() == 5


def test_get_height_feet_empty_string_returns_none():
    input_values = ['']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_feet() == None


# INPUT TESTING - GET HEIGHT INCHES
def test_get_height_inches_returns_valid_input():
    input_values = ['11']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_inches() == 11


def test_get_height_inches_ignores_zero():
    input_values = ['0', '5']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_inches() == 5


def test_get_height_inches_ignores_below_zero():
    input_values = ['-7', '10']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_inches() == 10


def test_get_height_inches_ignores_string_value():
    input_values = ['error', '9']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_inches() == 9


def test_get_height_inches_empty_string_returns_none():
    input_values = ['']

    def input(prompt=None):
        return input_values.pop()

    activity1.input = input
    assert activity1.get_height_inches() == None


# CONVERT HEIGHT TESTING
def test_convert_height_returns_inches():
    assert activity1.convert_height(1, 0) == 12
    assert activity1.convert_height(5, 11) == 71
    assert activity1.convert_height(8, 1) == 97


def test_convert_height_raises_value_error_on_non_numeric_value():
    with pytest.raises(ValueError):
        activity1.convert_height(float("X"), float("Y"))


def test_convert_height_raises_value_error_on_non_integer_ft_and_in_values():
    with pytest.raises(ValueError):
        activity1.convert_height(str("X"), str("Y"))


def test_convert_height_raises_value_error_if_feet_are_zero_or_below():
    with pytest.raises(ValueError):
        activity1.convert_height(0, 0)
    with pytest.raises(ValueError):
        activity1.convert_height(-5, 0)


def test_convert_height_raises_value_error_if_feet_are_ten_or_more():
    with pytest.raises(ValueError):
        activity1.convert_height(10, 0)
    with pytest.raises(ValueError):
        activity1.convert_height(100, 0)


def test_convert_height_raises_value_error_if_inches_are_zero_or_more():
    with pytest.raises(ValueError):
        activity1.convert_height(2, -5)


def test_convert_height_raises_value_error_if_inches_are_twelve_or_more():
    with pytest.raises(ValueError):
        activity1.convert_height(5, 12)
    with pytest.raises(ValueError):
        activity1.convert_height(6, 50)


# CALCULATE BMI TESTING
def test_calculate_bmi_returns_correct_bmi():
    assert activity1.calculate_bmi(125, 65) == 20.8
    assert activity1.calculate_bmi(250, 72) == 33.9


def test_calculate_bmi_raises_value_error_on_non_numeric_value():
    with pytest.raises(ValueError):
        activity1.calculate_bmi(float("X"), float("Y"))


def test_calculate_bmi_raises_value_error_on_non_integer_weight_value():
    with pytest.raises(ValueError):
        activity1.calculate_bmi(str("X"), str("Y"))


def test_calculate_bmi_raises_value_error_if_weight_is_zero_or_below():
    with pytest.raises(ValueError):
        activity1.calculate_bmi(0, 75)
    with pytest.raises(ValueError):
        activity1.calculate_bmi(-5, 62)


def test_calculate_bmi_raises_value_error_if_weight_is_999_or_more():
    with pytest.raises(ValueError):
        activity1.convert_height(999, 60)
    with pytest.raises(ValueError):
        activity1.convert_height(2500, 80)


def test_calculate_bmi_raises_value_error_if_inches_are_zero_or_below():
    with pytest.raises(ValueError):
        activity1.calculate_bmi(200, 0)
    with pytest.raises(ValueError):
        activity1.calculate_bmi(143, -125)


def test_calculate_bmi_raises_value_error_if_inches_are_108_or_more():
    with pytest.raises(ValueError):
        activity1.convert_height(250, 108)
    with pytest.raises(ValueError):
        activity1.convert_height(121, 2001)


# OUTPUT TESTING
def test_display_bmi_displays_results(capsys):
    activity1.display_bmi(15.2)
    captured = capsys.readouterr()
    assert captured.out == "\nYour BMI is 15.2.\n"


def test_display_bmi_raises_assertion_error_bmi():
    with pytest.raises(AssertionError):
        activity1.display_bmi("X")
