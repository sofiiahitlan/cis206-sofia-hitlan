"""This program calculates Body Mass Index(BMI) and provides BMI ranges
for user to determine if his/her BMI is in healthy range.

Input:
    Weight in pounds
    Height in feet and inches

Output:
    BMI
    BMI legend

Example:
    Please enter your weight in pounds:
    125

    Please enter your height in feet
    (remaining inches will be entered seperately):
    5

    Please enter remaining inches:
    6

    Your BMI is 20.2.

    Please refer to the legend to determine if your BMI is in healthy range.

    Below 18.5 - Underweight.
    18.5 to 24.9 - Normal.
    25.0 to 29.9 - Overweight.
    30.0 and Above - Obese.

References:
    * Formula: https://www.cdc.gov/healthyweight/assessing/bmi/
      childrens_bmi/childrens_bmi_formula.html
    * BMI ranges: https://www.bcbst.com/providers/MPMTools/
      BMICalculator.shtm
    * Decimal places: https://www.programiz.com/python-programming/methods/
      built-in/round
    * Functions syntax: https://bitbucket.org/sofiiahitlan/
      cis106-sofiia-hitlan/src/master/Final%20Project/finalproject.py
    * Python formatter:  http://pep8online.com/

"""

import os
import sys


def display_welcome():
    """Welcomes user to the program.

    Args: None

    Returns: None

    """
    print("Welcome to the Adult Body Mass Index Calculator! \n")


def get_pounds():
    """Gets weight in pounds.

    Args: None

    Returns:
        int: weight in pounds

    Exits:
        ValueError: If  weight is not a valid integer.
        ValueError: If weight is less than zero, zero or more than 999.

    """
    try:
        print("Please enter your weight in pounds:")
        weight_in_pounds = input()
        weight_in_pounds = int(weight_in_pounds)
        if weight_in_pounds <= 0 or weight_in_pounds > 999:
            print("Weight value should be more than 0 and less than 999 lbs.")
            print(f"ValueError: {weight_in_pounds} is invalid.")
            os._exit(1)

        return weight_in_pounds

    except ValueError:
        print("Weight value must be an integer.")
        print(f"ValueError: {weight_in_pounds} is invalid.")
        os._exit(2)


def get_height_feet():
    """Gets height in feet(whole number only).

    Args: None

    Returns:
        int: height in feet (whole number only)

    Exits:
        ValueError: If feet value  is not a valid integer.
        ValueError: If feet value is less than zero, zero or more than 9.

    """
    print("\nPlease enter your height in feet " +
          "(remaining inches will be entered seperately):")
    try:
        height_feet = input()
        height_feet = int(height_feet)
        if height_feet <= 0 or height_feet > 9:
            print("Height value should be more than 0 and less than 9.")
            print(f"ValueError: {height_feet} is invalid.")
            os._exit(1)

        return height_feet

    except ValueError:
        print("Feet value must be an integer." +
              " Remaining inches will be entered separately.")
        print(f"ValueError: {height_feet} is invalid.")
        os._exit(2)


def get_height_inches():
    """Gets the remaining inches of the height.

    Args: None

    Returns:
        int: inches

    Exits:
        ValueError: If inches value is not a valid integer.
        ValueError: If inches are is less than zero or more than 11.

    """
    try:
        print("\nPlease enter remaining inches:")
        height_inches = input()
        height_inches = int(height_inches)
        if height_inches < 0 or height_inches > 11:
            print("Inches value should be positive and can't exceed 11." +
                  " Please note that 12 inches make 1 feet.")
            print(f"ValueError: {height_inches} is invalid.")
            os._exit(1)

        return height_inches

    except ValueError:
        print("Inches value must be an integer.")
        print(f"ValueError: {height_inches} is invalid.")
        os._exit(2)


def convert_height(height_feet, height_inches):
    """Converts height from feet and inches to inches only.

    Args:
        feet (int): feet to be converted
        inches (int): inches to be converted

    Returns:
        int: height in inches

    Raises:
        ValueError: If feet value  is not a valid integer.
        ValueError: If feet value is less than zero, zero or more than 9.
        ValueError: If inches value is not a valid integer.
        ValueError: If inches are is less than zero or more than 11.
    """
    try:
        height_feet = int(height_feet)
    except ValueError:
        raise ValueError(f"Feet value must be an integer." +
                         " Received '{height_feet}'.")
    except:
        raise

    if height_feet <= 0 or height_feet > 9:
        raise ValueError(f"Feet value should be more than 0 and less than 9" +
                         ". Received {height_feet}.")

    try:
        height_inches = int(height_inches)
    except ValueError:
        raise ValueError(f"Inches value must be an integer." +
                          " Received '{height_inches}'.")
    except:
        raise

    if height_inches < 0 or height_inches > 11:
        raise ValueError(f"Inches value should be positive and can't exceed" +
                         " 11. Received '{height_inches}'.")

    height_in_inches = height_feet * 12 + height_inches

    return height_in_inches


def calculate_bmi(weight_in_pounds, height_in_inches):
    """Calculates BMI.

    Args:
        weight (int)
        height (int)

    Returns:
        int: BMI

    Raises:
        ValueError: If weight is not an integer.
        ValueError: If weight is less than 1 and more than 999 lbs.
        ValueError: If height is not an integer.
        ValueError: If height is less than 1 and more than 108 inches.

    """
    try:
        weight_in_pounds = int(weight_in_pounds)
    except ValueError:
        raise ValueError(f"Weight value should be an integer." +
                         " Received '{weight_in_pounds}'.")
    except:
        raise

    if weight_in_pounds <= 0 or weight_in_pounds > 999:
        raise ValueError(f"Weight value should be more than 0 and less than" +
                          " 999 lbs. Received '{weight_in_pounds}")

    try:
        height_in_inches = int(height_in_inches)
    except ValueError:
        raise ValueError(f"Height value should be an integer." +
                         " Received '{height_in_inches}'.")
    except:
        raise

    if height_in_inches < 1 or height_in_inches > 108:
        raise ValueError(f"Height value should be more than 0 and less than" +
                         " 108 inches. Received '{height_in_inches}")

    bmi = 703 * weight_in_pounds / (height_in_inches * height_in_inches)

    return bmi


def display_bmi(bmi):
    """Displays BMI.

    Args: bmi (float)

    Returns: None

    AssertionError: If BMI is not a valid float.

    """
    assert isinstance(bmi, float) or isinstance(bmi, int), \
        "BMI must be a float. Received {type(BMI)}"
    print("\nYour BMI is " + str(round(bmi, 1)) + ".")


def display_bmi_range():
    """Displays BMI legend.

    Args: None

    Returns: None

    """
    print("\nPlease refer to the legend to determine if your BMI is" +
          " in healthy range.\n")
    print("Below 18.5 - Underweight. \n18.5 to 24.9 - Normal. " +
          "\n25.0 to 29.9 - Overweight. \n30.0 and Above - Obese.")


def main():
    """Runs the main program logic."""

    try:
        weight_in_pounds = get_pounds()
        height_feet = get_height_feet()
        height_inches = get_height_inches()
        height_in_inches = convert_height(height_feet, height_inches)
        bmi = calculate_bmi(weight_in_pounds, height_in_inches)
        display_bmi(bmi)
        display_bmi_range()
    except:
        print("Unexpected error.")
        print("Error:", sys.exc_info()[1])
        print("File: ", sys.exc_info()[2].tb_frame.f_code.co_filename)
        print("Line: ", sys.exc_info()[2].tb_lineno)


main()
