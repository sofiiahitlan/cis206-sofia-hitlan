import gzip
import zlib
import re

wikimedia_data = (r'C:\Users\coni4\Desktop\College\c'
                  r'is206 Applied Programming'
                  r'\cis206-sofia-hitlan\practice\p'
                  r'ageviews-20200302-100000.gz')

log_file_pattern = re.compile(r'(en\.v)\s([^\s]+)\s(\d+)\s(\d+)')
    
with gzip.open(wikimedia_data, 'r') as file:
    wikiversity_records = {}
    for line in file:
        line = line.strip()
        record = str(line, encoding="utf-8")
        matches = log_file_pattern.finditer(record)
        
        for match in matches:
            wikiversity_records[match.group(2)] = int(match.group(3))

    print(wikiversity_records)