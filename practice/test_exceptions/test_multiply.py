import pytest
import multiply


def test_invalid_input():
    input_values = [
        "2",
        "X"
    ]

    def input(prompt=None):
        return input_values.pop()

    multiply.input = input

    with pytest.raises(ValueError):
        multiply.main()