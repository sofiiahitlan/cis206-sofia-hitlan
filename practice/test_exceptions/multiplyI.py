def multiply(x, y):
    result = x * y
    return result


def main():
    multiplicand = float(input("Enter a multiplicand: "))
    multiplier = float(input("Enter a multiplier: "))
    product = multiply(multiplicand, multiplier)
    print(f"The product is: {product}")


if __name__ == "__main__":
    main()