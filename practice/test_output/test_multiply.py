import multiply

def test_output(capsys):
    input_values = [
        "2",
        "2"
    ]

    def input(prompt=None):
        return input_values.pop()

    multiply.input = input
    assert multiply.main() == None
    captured = capsys.readouterr()
    assert captured.out == "The product is: 4.0\n"