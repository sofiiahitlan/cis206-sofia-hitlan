"""This file tests the Fahrenheit temperature conversion program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    8 passed in 0.xx seconds

References:
    * https://realpython.com/python-testing/
    * http://docs.pytest.org/en/latest/getting-started.html

"""
import pytest
import temperature


def test_get_fahrenheit_returns_valid_input():
    input_values = ['100']

    def input(prompt=None):
        return input_values.pop()

    temperature.input = input
    assert temperature.get_fahrenheit() == 100


def test_get_fahrenheit_ignores_below_absolute_zero():
    input_values = ['-500', '0']

    def input(prompt=None):
        return input_values.pop()

    temperature.input = input
    assert temperature.get_fahrenheit() == 0


def test_get_fahrenheit_ignores_string_value():
    input_values = ['error', '0']

    def input(prompt=None):
        return input_values.pop()

    temperature.input = input
    assert temperature.get_fahrenheit() == 0


def test_get_fahrenheit_empty_string_returns_none():
    input_values = ['']

    def input(prompt=None):
        return input_values.pop()

    temperature.input = input
    assert temperature.get_fahrenheit() == None


def test_fahrenheit_to_celsius_returns_celsius():
    assert temperature.fahrenheit_to_celsius(98.6) == 37
    assert temperature.fahrenheit_to_celsius(32) == 0
    assert temperature.fahrenheit_to_celsius(-40) == -40
    assert round(temperature.fahrenheit_to_celsius(-459.67), 2) == -273.15


def test_fahrenheit_to_celsius_raises_value_error_on_non_numeric_value():
    with pytest.raises(ValueError):
        temperature.fahrenheit_to_celsius(float("X"))


def test_fahrenheit_to_celsius_raises_value_error_below_absolute_zero():
    with pytest.raises(ValueError):
        temperature.fahrenheit_to_celsius(-459.68)


def test_display_results_displays_results(capsys):
    temperature.display_results(32, 0)
    captured = capsys.readouterr()
    assert captured.out == "32.0° Fahrenheit is 0.0° Celsius\n\n"


def test_display_results_raises_assertion_error_fahrenheit():
    with pytest.raises(AssertionError):
        temperature.display_results("X", 0)


def test_display_results_raises_assertion_error_celsius():
    with pytest.raises(AssertionError):
        temperature.display_results(0, "X")