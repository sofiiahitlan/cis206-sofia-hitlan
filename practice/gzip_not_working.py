'''
References: 
    * sorting dictionary by value: 
        1) https://docs.python.org/3/howto/sorting.html#operator-module
           -functions
        2) https://stackoverflow.com/questions/613183/how-do-i-sort-a-
           dictionary-by-value
    * open all .txt files in the directory:
        https://stackoverflow.com/questions/3964681/find-all-files-in-a
        -directory-with-extension-txt-in-python
'''
# import csv
import glob
import gzip
import re
import os
from operator import itemgetter




def get_data(my_directory, wikimedia_data):
    """ Reads the file.

    Args: wikimedia_data (file)

    Returns: wikiversity_records(dict)
    """
    log_file_pattern = re.compile(r'(en\.v)\s([^\s]+)\s(\d+)\s(\d+)')
    
    try:
        for path, directories, files in os.walk(os.getcwd()):
            for directory in directories:
                if os.path.join(path, directory) == my_directory:
                    print(directory)
                    print("WORKS!!!")
                    for file in glob.glob("./*.gz"):
                        print(file)
                        with open(wikimedia_data, "r") as file:
                            wikiversity_records = {}
                            for line in file:
                                line = line.strip()
                                matches = log_file_pattern.finditer(line)
                                
                                for match in matches:
                                    wikiversity_records[match.group(2)] = int(match.group(3))

                            print(wikiversity_records)

                        return wikiversity_records
        

        # for file in glob.glob("./*.txt"):
        #     print(file)
            

    except FileNotFoundError:
        print("File was not found.")
        os._exit(1)


def sort_data(wikiversity_records):
    """ Sorts Wikiversity pages in descending order based on the
    number of visits.

    Args:
        wikiversity_records (dict)

    Returns:
        none
    """
    try:
        if type(wikiversity_records) is not dict:
            raise AttributeError
        
        wikiversity_records = sorted(wikiversity_records.items(), key=itemgetter(1), reverse=True)

        record_count = 0
        for key, value in wikiversity_records:
            print(key + ":", value)
            record_count += 1
            if record_count > 100:
                break
    
    except AttributeError:
        print("AttributeError: dictionary not of class type dict.")
        print("Received '%s' of type %s." % (wikiversity_records, type(wikiversity_records)))


# def find_projects(wikiversity_records):
#     project_pattern = re.compile(r'(en.v)\s(\w+)\/(\w+)\s(\d+)\s(\d+)')
#     projects = {}
#     for record in wikiversity_records:
#         for key, value in record:
#             matches = project_pattern.finditer(record)
#             for match in matches:
#                 title = match.group(2)
#                 projects[title] = 





def main():
    """ Runs the main program logic. """
    my_directory = (r'C:\Users\coni4\Desktop\College\c'
                    r'is206 Applied Programming'
                    r'\cis206-sofia-hitlan\assignment10')
    wikimedia_data = (r'C:\Users\coni4\Desktop\College\c'
                      r'is206 Applied Programming'
                      r'\cis206-sofia-hitlan\assignment10\s'
                      r'ample_data1.txt')
    # wikimedia_data = 'pageviews-20200301-160000.gz'
    # wikimedia_data = (r'C:\Users\coni4\Desktop\College\c'
    #                   r'is206 Applied Programming'
    #                   r'\cis206-sofia-hitlan\assignment10\p'
    #                   r'ageviews-20200302-100000.gz')
    wikiversity_records = get_data(my_directory, wikimedia_data)
    sort_data(wikiversity_records)



if __name__ == "__main__":
    main()
