import multiply

def test_input():
    input_values = [
        "2",
        "2"
    ]

    def input(prompt=None):
        return input_values.pop()

    multiply.input = input
    assert multiply.main() == None