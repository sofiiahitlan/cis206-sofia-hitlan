"""This file tests database program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    x passed in 0.xx seconds
"""
import pytest
import re
import sqlite3
import database


connection = sqlite3.connect("Northwind.db")
cursor = connection.cursor()

list_of_table_tuples = [('Regions',), ('Territories',), ('Suppliers',),
                        ('Categories',), ('Products',), ('Employees',),
                        ('Customers',), ('Orders',), ('OrderDetails',),
                        ('InternationalOrders',),
                        ('EmployeesTerritories',)]

tables = ['Regions', 'Territories', 'Suppliers', 'Categories',
          'Products', 'Employees', 'Customers', 'Orders', 'OrderDetails',
          'InternationalOrders', 'EmployeesTerritories']

regions_records = [(1, 'Eastern'), (2, 'Western'), (3, 'Northern'),
                   (4, 'Southern')]

region_tuple = (('RegionID', None, None, None, None, None, None),
                ('RegionDescription', None, None, None, None, None, None))

sizes_array = [7, 8, 17]


def test_connect_to_database_connects_successfuly():
    database.connect_to_database("test.db")


# def test_connect_to_database_raises_when_fails():
#     # database.connect_to_database("")
#     with pytest.raises():
#         database.connect_to_database("")


def test_create_cursor_creates_cursor():
    cursor = database.create_cursor(connection)
    hasattr(cursor, 'description')


def test_create_list_of_table_tuples_returns_correct_list():
    assert database.create_list_of_table_tuples(cursor,
                                                connection) == list_of_table_tuples


def test_create_list_of_tables_returns_correct_list():
    assert database.create_list_of_tables(list_of_table_tuples) == tables


def test_get_table_choice_returns_correct_integer():

    input_values = [1]

    def input(prompt=None):
        return input_values.pop()

    database.input = input
    assert database.get_table_choice(tables) == 1


def test_determine_table_name_returns_correct_name():
    assert database.determine_table_name(1, tables) == 'Regions'
    assert database.determine_table_name(2, tables) == 'Territories'
    assert database.determine_table_name(9, tables) == 'OrderDetails'


def test_determine_table_columns_returns_correct_tuple():
    assert database.determine_table_columns(cursor, 'Regions') == region_tuple


def test_build_fields_array_returns_correct_list():
    assert database.build_fields_array(region_tuple) == ['RegionID',
                                                         'RegionDescription']


def test_determine_column_name_size_returns_correct_size():
    assert database.determine_column_name_size("LastName") == 8
    assert database.determine_column_name_size("No.") == 3
    assert database.determine_column_name_size("Phone_Number") == 12


def test_find_table_records_returns_correct_list_of_record_tuples():
    assert database.find_table_records("Regions",
                                       tables, cursor) == regions_records


def test_determine_max_record_length_returns_correct_length():
    assert database.determine_max_record_length(cursor, "RegionID", "Regions",
                                                regions_records) == 1


def test_create_sizes_array_returns_correct_array():
    assert database.create_sizes_array(cursor, "Regions", regions_records,
                                       ["RegionID",
                                        "RegionDescription"]) == sizes_array


def test_display_table_records_display_output_correctly():
    database.display_table_records("Regions", ["RegionID",
                                   "RegionDescription"],
                                   regions_records, sizes_array) == """
                                   'Regions' table records:

                                    Row No. RegionID RegionDescription
                                    1       1        Eastern
                                    2       2        Western
                                    3       3        Northern
                                    4       4        Southern"""


def test_close_connection_closes_connection():
    assert database.close_connection(connection) is True


def test_close_connection_returns_False_if_can_not_close_connection():
    assert database.close_connection("") is False
