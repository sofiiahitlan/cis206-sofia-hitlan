import sqlite3

connection = sqlite3.connect("Northwind.db")
cursor = connection.cursor()

table_name = "Regions"

fields = ['RegionID', 'RegionDescription']

records = [(1, 'Eastern'), (2, 'Western'), (3, 'Northern'), (4, 'Southern')]
field_sizes = []

for field in fields:
    column_name_size = len(field)
    print(f"field called {field} has a length of {column_name_size}")
    cursor.execute(f"SELECT MAX(LENGTH({field})) FROM {table_name};")
    longest_field_size = cursor.fetchall()
    longest_field_size = longest_field_size[0][0]
    print(f"Field callled {field} has a record with a max size of {longest_field_size}")

    if column_name_size >= longest_field_size:
        column_size = column_name_size
    else:
        column_size = longest_field_size
    field_sizes.append(column_size)
    field = field.ljust(column_size)
    print(f"Final column size is {column_size}\n")
    
print("WIDTHS:", field_sizes)

for j in range(0, len(fields)):
    print(fields[j], end = " ")
print()
    
for record in records:
#    print("RECORD", record)
    for i in range(0, len(record)):
#        print("I", record[i])
        element = str(record[i])
        element = element.ljust(field_sizes[i])
        print(element, end=" ")
    print()
#print(records)
#print(fields)
#
#for field in fields:
#    print(field, end=" ")
#print()
#for record in records:
#    for column in record:
#        print(column, end = " ")
#    print()
    
#    i = 0
#    print("OUTPUT")
#    print(field.ljust(column_size), end=" ")
#    print()
    #print(str(fields[i]).ljust(column_size), end=" ")

    


    


#for record in records:
#        for column in record:
#            column = str(column)
#            column = column.ljust(column_size)
#            print(column, end=" ")
#        print()