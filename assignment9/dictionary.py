""" This program reads northwind_customers.txt line by line, creating a
list of records. Each customer records becomes a dictionary.

Users have an option to sort dictionary by key chosen and print sorted
records (choices 1 and 2). User can also search for the company by its
name or contact name. (choices 3 and 4).

Input:
    user's choice (4 choices offered)

Output:
    records (sorted)
    OR
    records (that meet searching criteria)

Example if option #1 has been chosen by user:

    Choose one of the following options:
    Press 1 to sort customers by company name.
    Press 2 to sort customers by contact name.
    Press 3 to search for company by full or partial name.
    Press 4 to search for contact person by full or partialname.

    1

    Alfreds Futterkiste     Maria Anders    030-0074321
    Ana Trujillo Emparedados y helados      Ana Trujillo    (5) 555-4729

Example if option #3 has been chosen by user:

    Choose one of the following options:
    Press 1 to sort customers by company name.
    Press 2 to sort customers by contact name.
    Press 3 to search for company by full or partial name.
    Press 4 to search for contact person by full or partialname.

    3

    Enter company name you want so search for:
    ana

    CustomerID: ANATR, CompanyName: Ana Trujillo Emparedados y helados,
    ContactName: Ana Trujillo, ContactTitle: Owner, Address: Avda. de
    la ConstituciÃ³n 2222, City: MÃ©xico D.F., Region: , PostalCode:
    05021, Country: Mexico, Phone: (5) 555-4729, Fax: (5) 555-3745,

Reference:
    * Dictionary gets overwritten every time the loop runs:
        stackoverflow.com/questions/12753099/filling-a-python-
        dictionary-in-for-loop-returns-same-values
"""
import os
import re


def get_labels(northwind_customers):
    """ Reads first line of file and created a list of headers for future use.
    They will be used keys in the dictionary.

    Args: northwind_customers (file)

    Returns: labels (list)
    """
    try:
        with open(northwind_customers, "r") as file:
            labels = []
            first_line = file.readline()
            first_line = first_line.strip()
            first_line = first_line.split(",")

            for index in range(len(first_line)):
                    first_line[index] = first_line[index].strip("\"")

            labels = first_line

            return labels
    except FileNotFoundError:
        print("File was not found.")
        # os._exit(1)


def get_records(northwind_customers, labels):
    """ Reads the file, strips each line, makes each line a list,
    seperates each list with a comma, removes the first line containing
    headers from the list.

    Args:
        northwind_customers (str): path to the file
        labels (list): headers

    Returns:
        northwind_customers

    Raises:
        ValueError: If no path to file with customers was provided
    """
    try:
        with open(northwind_customers, "r") as file:
            records = []
            for line in file:
                line = line.strip()

                line = re.split('"(.*?)"', line)
                for index in range(len(line) - 1, -1, -2):
                    del line[index]

                single_record = {}
                for index in range(len(line)):
                    single_record[labels[index]] = line[index]
                records.append(single_record)

                # print(single_record)

        # print("Records after reading file line by line:")
        # print(records)

        records.pop(0)

        return records

    except FileNotFoundError:
        print("File was not found.")
        # os._exit(1)


def get_choice():
    """ Gets the user choice. 4 options offered.

    Args: none

    Returns: choice (str)

    Raises:

    """
    while True:
        try:
            print("Choose one of the following options:")
            print("Press 1 to sort customers by company name.")
            print("Press 2 to sort customers by contact name.")
            print("Press 3 to search for company by full or partial name.")
            print("Press 4 to search for contact person by full or partial" +
                  "name.")

            choices = ["1", "one", "One", "2", "two", "Two",
                       "3", "three", "Three", "4", "four", "Four"]

            choice = input()
            if choice in choices and choice != "":
                return choice
            else:
                print("Invalid input. Press any key to correct your input " +
                      "or press Enter to quit now.")
                exit_condition = input()

                if exit_condition == "":
                    os._exit(1)
                # return choice
        except ValueError:
            if choice == "":
                return None
            else:
                print("Invalid input.")
                print("Your options are: \"1\", \"2\", \"3\", \"4\"")
                print("Press any key to correct your input or enter" +
                      "\"exit\" to quit now.")

                exit_condition = input()
                if exit_condition == "exit" or exit_condition == "Exit":
                    os._exit(1)


def process_choice(choice, records, labels):
    """ Assigns a key based on user's choice. Key comes from the key-value
    pair in a dictionary. Picks others keys that together with the key
    will be displayed later. Also, based on the choice, calls either
    sort_customers() or search_customers() function.

    Args:
        choice (str)
        records (list of dictionaries)
        labels (list of headers)

    Returns:
        none

    """
    if choice == "1" or choice == "one" or choice == "One":
        key = "CompanyName"
        sort_customers(records, key, ["CompanyName", "ContactName", "Phone"])
    elif choice == "2" or choice == "two" or choice == "Two":
        key = "ContactName"
        sort_customers(records, key, ["ContactName", "CompanyName", "Phone"])
    elif choice == "3" or choice == "three" or choice == "Three":
        key = "CompanyName"
        search_customers(records, "company name", key, labels)
    elif choice == "4" or choice == 4 or choice == "four" or choice == "Four":
        key = "ContactName"
        search_customers(records, "contact person name", key, labels)
    else:
        print("Invalid choice.")


def sort_customers(records, key, keys):
    """ Sorts customers based on the key.

    Args:
        records (list of dictionaries)
        key (str)
        keys (str)

    Returns:
        none
    """
    records = sorted(records, key=lambda record: record[key])
    for record in records:
        for key in keys:
            print(record[key], end="\t")
        print()


def search_customers(records, name, key, labels):
    """ Search customers based on the key.

    Args:
        records (list of dictionaries)
        name (literal str):
        key (str)
        labels (list)

    Returns:
        none
    """
    print(f"Enter {name} you want so search for:")
    search_query = input()
    try:
        # print("Results:\n")
        is_match = False
        for record in records:
            i = 0
            if search_query.lower() in record[key].lower():
                is_match = True
                for field in record:
                    print(labels[i], end=": ")
                    if field != "Fax":
                        print(record[field], end=", ")
                    else:
                        print(record[field], end=".")
                    i += 1
                print("\n")
        if is_match is False:
            print("No match found.")
    except ValueError:
        print("Error. Exiting.")
        os._exit(1)


def main():
    """ Runs the main program logic. """
    northwind_customers = 'northwind_customers.txt'
    # northwind_customers = (r'C:\Users\coni4\Desktop\College\c'
    #                        r'is206 Applied Programming'
    #                        r'\cis206-sofia-hitlan\assignment9\n'
    #                        r'orthwind_customers.txt')
    labels = get_labels(northwind_customers)
    records = get_records(northwind_customers, labels)
    while True:
        choice = get_choice()
        if choice not in "1234":
            break

        process_choice(choice, records, labels)


if __name__ == "__main__":
    main()
