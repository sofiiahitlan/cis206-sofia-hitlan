"""This file tests the Northwind customers sort and search program
using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    6 passed in 0.xx seconds

"""
import pytest
import dictionary

northwind_customers = 'northwind_customers.txt'
# northwind_customers = (r'C:\Users\coni4\Desktop\College\c'
#                        r'is206 Applied Programming'
#                        r'\cis206-sofia-hitlan\n'
#                        r'orthwind_customers.txt')

labels = ['CustomerID', 'CompanyName', 'ContactName', 'ContactTitle',
          'Address', 'City', 'Region', 'PostalCode', 'Country', 'Phone', 'Fax']

records = [{
                "CustomerID": "ALFKI", "CompanyName": "Alfreds Futterkiste",
                "ContactName": "Maria Anders", "ContactTitle":
                "Sales Representative", "Address": "Obere Str. 57",
                "City": "Berlin", "Region": "", "PostalCode": "12209",
                "Country": "Germany", "Phone": "030-0074321",
                "Fax": "030-0076545"},
           {
                "CustomerID": "ANATR", "CompanyName":
                "Ana Trujillo Emparedados y helados", "ContactName":
                "Ana Trujillo", "ContactTitle": "Owner", "Address":
                "Avda. de la Constitución 2222", "City": "México D.F.",
                "Region": "", "PostalCode": "05021", "Country": "Mexico",
                "Phone": "(5) 555-4729", "Fax": "(5) 555-3745"}]


def test_get_labels_creates_list_of_headers():
    assert dictionary.get_labels(northwind_customers) == labels


def test_get_labels_prints_message_if_file_not_found(capsys):
    dictionary.get_labels("non_existing_file.txt")
    captured = capsys.readouterr()
    assert captured.out == "File was not found.\n"


def test_get_records_prints_message_if_file_not_found(capsys):
    dictionary.get_records("non_existing_file.txt", labels)
    captured = capsys.readouterr()
    assert captured.out == "File was not found.\n"


def test_get_records_returns_array():
    assert type(dictionary.get_records(northwind_customers, labels)) is list


def test_get_choice_returns_valid_input():
    input_values = ["one"]

    def input(prompt=None):
        return input_values.pop()

    dictionary.input = input
    assert dictionary.get_choice() == "one"


def test_get_choice_returns_error_message_on_invalid_input():
    input_values = ["5", "5", "1"]

    def input(prompt=None):
        return input_values.pop()

    dictionary.input = input
    assert dictionary.get_choice() == "1"


# # def test_get_choice_empty_string_returns_none():
# #     input_values = [""]

# #     def input(prompt=None):
# #         return input_values.pop()

# #     dictionary.input = input
# #     assert dictionary.get_choice() == None


def test_process_choice_returns_None():
    assert dictionary.process_choice(1, records, labels) is None


def test_sort_customers_returns_correct_results(capsys):
    dictionary.sort_customers(records, "CompanyName",
                              ["CompanyName", "ContactName", "Phone"])
    captured = capsys.readouterr()
    assert captured.out == ("Alfreds Futterkiste\tMaria Anders\t030-0074321" +
                            "\t\nAna Trujillo Emparedados y helados" +
                            "\tAna Trujillo\t(5) 555-4729\t\n")


def test_search_customers_returns_correct_results_if_match_is_found(capsys):
    input_values = ["ana"]

    def input(prompt=None):
        return input_values.pop()

    dictionary.input = input

    dictionary.search_customers(records, "company name", "CompanyName", labels)
    captured = capsys.readouterr()

    assert captured.out == ("Enter company name you want so search for:" +
                            "\nCustomerID: ANATR, CompanyName: Ana Trujillo " +
                            "Emparedados y helados, ContactName: Ana " +
                            "Trujillo, ContactTitle: Owner, Address: Avda." +
                            " de la Constitución 2222, City: México " +
                            "D.F., Region: , PostalCode: 05021, Country: " +
                            "Mexico, Phone: (5) 555-4729, Fax: (5) 555-3745" +
                            ".\n\n")


def test_search_customers_prints_message_if_no_match_found(capsys):
    input_values = ["zone"]

    def input(prompt=None):
        return input_values.pop()

    dictionary.input = input

    dictionary.search_customers(records, "company name", "CompanyName", labels)
    captured = capsys.readouterr()

    assert captured.out == ("Enter company name you want so search for:\nNo" +
                            " match found.\n")
