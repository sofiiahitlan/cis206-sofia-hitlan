#References:
#Formula: https://www.cdc.gov/healthyweight/assessing/bmi/childrens_bmi/childrens_bmi_formula.html
#BMI ranges: https://www.bcbst.com/providers/MPMTools/BMICalculator.shtm
#Decimal places: https://www.programiz.com/python-programming/methods/built-in/round

print("Welcome to the Adult Body Mass Index Calculator! \n")

print("Please enter your weight in pounds:")
weight_in_pounds = int(input())

print("\nPlease enter your height in feet (remaining inches will be entered seperately):")
height_feet = int(input())

print("\nPlease enter remaining inches:")
height_inches = int(input())

height_in_inches = height_feet * 12 + height_inches

bmi = 703 * weight_in_pounds / (height_in_inches * height_in_inches)

print("\nYour BMI is " + str(round(bmi, 1)) + ".")

print("\nPlease refer to the legend to determine if your BMI is in healthy range.\n")
print("Below 18.5 - Underweight. \n18.5 to 24.9 - Normal. \n25.0 to 29.9 - Overweight. \n30.0 and Above - Obese.")


