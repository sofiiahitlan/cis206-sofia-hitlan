"""This program converts the string of alphabetic characters to
   a run-length encoded (RLE) string of characters and numbers.

Input:
    string

Output:
    final_string (encoded or decoded)

Example (encoding):
    Enter a string you would like to encode or decode:
    aaaaaaaBBBccccccDDDDDDD

    After convertation your string is:
    a7B3c6D7

Example (decoding):
    Enter a string you would like to encode or decode:
    a3Bc12M

    After convertation your string is:
    aaaBccccccccccccM

References:
    * Run-length encoding: https://stackabuse.com/run-length-encoding/
    * RegEx re.search method: https://docs.python.org/3/library/
                              re.html#re.search
    * string.isdigit() method: https://docs.python.org/3/library/
                               stdtypes.html#str.isdigit

"""

import re
import sys


def get_string():
    """Gets string.

    Args: None

    Returns:
        str: Text string entered or None if no string entered.

    """
    while True:
        print("Enter a string you would like to encode or decode:")
        string = input()

        if string == "":
            return None
        else:
            return string


def determine_string_type(string):
    """Determines if string needs to be encoded or decoded.

    Args:
        string(str): either encoded or decoded string

    Returns:
        None.

    """
    if re.search(r"\d", string):
        final_string = decode_string(string)
    else:
        final_string = encode_string(string)

    display_results(final_string)


def encode_string(string):
    """Converts the string to a run-length encoded string of
       characters and numbers.

    Args:
        string (str): string to be encoded

    Returns:
        str: encoded_string

    """
    encoded_string = ""
    previous_character = ""
    count = 0

    for element in string:
        while element != previous_character:
                if previous_character != "":
                        if count == 1:  # compressed format: count is not
                                        # printed for a single instance of
                                        # a character
                            encoded_string += previous_character
                        else:
                            encoded_string += previous_character + str(count)

                count = 0
                previous_character = element
        count += 1

    if count == 1:  # compressed format: see description above
        encoded_string += previous_character
    else:
        encoded_string += previous_character + str(count)

    return encoded_string


def decode_string(string):
    """Converts RLE string to a string of alphabetic characters.

    Args:
        string (str): string to be decoded

    Returns:
        str: decoded_string

    """
    decoded_string = ""
    i = 0

    for element in string:
        if element.isdigit():
            i += 1
        else:
            if (i + 1) < len(string) and string[i + 1].isdigit():
                if (i + 1) < len(string) and string[i + 2].isdigit():
                    decoded_string += element * int(string[i + 1] +
                                                    string[i + 2])
                else:
                    decoded_string += element * int(string[i + 1])
                i += 1
            else:
                decoded_string += element
                i += 1

    return decoded_string


def display_results(final_string):
    """Displays encoded or decoded string.

    Args: final_string (str)

    Returns: None

    """
    print(f"\nAfter convertation your string is:\n{final_string}")


def main():
    """Runs the main program logic."""

    string = get_string()
    determine_string_type(string)


main()
