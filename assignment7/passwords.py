"""
This program prompts the user for a password, and then determines
and prints a level of its strength. It checks if password matches any
words that come from dictionary, any commonly used passwords,
any passwords previously used by user.

Input:
    password (not echoed)

Output:
    strength
    dictionary_match
    common_passwords_match,
    is_unique

Example:
    Enter your password:
    Results:

    Based on the length and types of characters used, your password is:
    very weak.

    Common words or phrases were detected. You are susceptible to a
    dictionary attack.

    Your password is common. You are susceptible to a common password
    attack.

    Your current password matches one of your previously used passwords.
    Do not reuse same password for different accounts.
References:
    * Regex patterns source: https://www.regextester.com/
    * RegEx re.search method: https://docs.python.org/3/library/
                              re.html#re.search
    * flags = re.IGNORECASE: https://stackoverflow.com/questions/
                             500864/case-insensitive-regular-expression
                             -without-re-compile
"""
import getpass
import re
import os
import sys


def get_password():
    """Gets a password.

    Args: None

    Returns:
        str: password or None if no password entered.

    """
    while True:
        try:
            password = getpass.getpass(prompt='Enter your password: ')
            return password
        except ValueError:
            if password == "":
                return None

# HARD TO TEST WHEN RELATIVE PATHS NOT WORKING ON MY SIDE
# def set_customer_passwords_path():
#     customer_passwords_path = ('customer_passwords.txt')
#     return customer_passwords_path


def check_uniqueness(password):
    """Checks if the password entered by customer has never been used
    by him or her before.

    Args:
        password (str)

    Returns:
        customer_passwords_path (str): location of the file
        password_length (int)

    Raises:
        ValueError: If no path to file with passwords was provided
    """
    customer_passwords_path = ('customer_passwords.txt')
    customer_passwords_path = (r'C:\Users\coni4\Desktop\Coll'
                               r'ege\cis206 Applied Programming\file'
                               r's\customer_passwords.txt')
    try:
        with open(customer_passwords_path, "r") as file:
            for line in file:
                line = line.strip()
                if password == line:
                    is_unique = False
                    break
                else:
                    is_unique = True
    except ValueError:
        print("File was not found.")
    return is_unique


def append_password(password):
    """Appends users password to the text file.

    Args:
        password (str)

    Returns:
        None

    """
    customer_passwords_path = ('customer_passwords.txt')

    with open(customer_passwords_path, "a") as file:
        file.write(password)
        file.write("\n")


def calculate_length(password):
    """Calculates the length of entered password.

    Args:
        password (str)

    Returns:
        password_length (int)
    """
    password_length = len(password)
    return password_length


def find_character_type_strength(password):
    """Calculates the strength of the password based on how many
    types of character it consists of.

    4 possible types of characters:
        * numbers
        * lowercase letters
        * uppercase letters
        * special characters (punctuation, etc.).

    Args:
        password (str)

    Returns:
        password_length (int)
    """
    numeric_count = 0
    special_character_count = 0
    lowercase_count = 0
    uppercase_count = 0

    for char in password:
        if re.search(r"\d", char):
            numeric_count += 1
        elif re.search(r"[a-z]", char):
            lowercase_count += 1
        elif re.search(r"[A-Z]", char):
            uppercase_count += 1
        elif re.search(r"[^A-Za-z0-9]", char):
            special_character_count += 1
        else:
            print("Problem recognizing an input character.")

    strength_count = 0

    if numeric_count > 0:
        strength_count += 1
    if special_character_count > 0:
        strength_count += 1
    if lowercase_count > 0:
        strength_count += 1
    if uppercase_count > 0:
        strength_count += 1

    return strength_count


def determine_strength(password_length, strength_count):
    """Determines an overal strength of the password.

    4 possible levels of strength:
        * very weak
        * wean
        * strong
        * very strong.

    Args:
        password_length (int)
        strength_count (int)

    Returns:
        strength (str)
    """
    if password_length >= 12 and strength_count == 4:
        strength = "very strong"
    elif password_length >= 12 and strength_count == 3:
        strength = "strong"
    elif password_length >= 12 and strength_count == 2:
        strength = "weak"
    elif password_length >= 12 and strength_count == 1:
        strength = "very weak"
    else:
        strength = "very weak"

    return strength


def check_dictionary(password):
    """Reads dictionary file line by line and determines if words from
    it are present in a password. Search is case-insensitive.

    Args:
        password (str)

    Returns:
        dictionary_match (Bool)
    """
    dictionary_path = ('words.txt')
    # dictionary_path = (r'C:\Users\coni4\Desktop\College\cis'
    #                    r'206 Applied Programming\files\words.txt')

    with open(dictionary_path, "r") as file:
        for line in file:
            line = line.strip()
            dictionary_match = False
            if re.search(rf"\b\w*{line}\w*\b", password, flags=re.IGNORECASE):
                dictionary_match = True
                break

    return dictionary_match


def check_common_passwords(password):
    """Reads common passwords file line by line and determines if there is
    a match with a user's password. Search is case-insensitive.

    Args:
        password (str)

    Returns:
        common_passwords_match (Bool)
    """
    common_passwords_path = ('passwords.txt')
    # common_passwords_path = (r'C:\Users\coni4\Desktop\College\cis'
    #                          r'206 Applied Programming\files\passwords.txt')

    with open(common_passwords_path, "r") as file:
        for line in file:
            line = line.strip()
            if re.search(rf"\b\w*|W*{line}\w*|W*\b", password,
                         flags=re.IGNORECASE):
                common_passwords_match = True
                break
            else:
                common_passwords_match = False

    return common_passwords_match


def print_results(strength, dictionary_match, common_passwords_match,
                  is_unique):
    """Displays password strength.

    Args: strength (str)

    Returns: None
    """
    print("Results:")
    print(f"\nBased on the length and types of characters used, " +
          f"your password is: {strength}.")

    if dictionary_match is True:
        print("\nCommon words or phrases were detected. " +
              "You are susceptible to a dictionary attack.")
    else:
        print("\nCommon words or phrases were not detected. " +
              "You are not currently susceptible to a dictionary attack.")

    if common_passwords_match is True:
        print("\nYour password is common. " +
              "You are susceptible to a common password attack.\n")
    else:
        print("\nYour password is not currently susceptible to a common " +
              "password attack.\n")

    if is_unique is False:
        print("Your current password matches one of your previously used " +
              "passwords. Do not reuse same password for different accounts.")
    else:
        print("Good job using unique passwords!")


def main():
    while True:
        try:
            password = get_password()
            is_unique = check_uniqueness(password)
            append_password(password)
            password_length = calculate_length(password)
            strength_count = find_character_type_strength(password)
            strength = determine_strength(password_length, strength_count)
            dictionary_match = check_dictionary(password)
            common_passwords_match = check_common_passwords(password)
            print_results(strength, dictionary_match, common_passwords_match,
                          is_unique)
        except:
            print("Unexpected error.")
            print("Error:", sys.exc_info()[1])
            print("File: ", sys.exc_info()[2].tb_frame.f_code.co_filename)
            print("Line: ", sys.exc_info()[2].tb_lineno)


if __name__ == "__main__":
    main()
