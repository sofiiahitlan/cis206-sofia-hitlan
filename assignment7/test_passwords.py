"""This file tests the password checker program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    7 passed in 0.xx seconds
"""
import pytest
import passwords


def test_get_password_returns_valid_input():
    input_values = ['password']

    def input(prompt=None):
        return input_values.pop()

    passwords.getpass.getpass = input
    assert passwords.get_password() == 'password'


# NOT WORKING - DON'T KNOW WHY:

# def test_get_password_empty_string_returns_none():
#     input_values = ['']

#     def input(prompt=None):
#         return input_values.pop()

#     passwords.getpass.getpass = input
#     assert passwords.get_password() == None


def test_check_uniqueness_returns_correct_Boolean():
    assert passwords.check_uniqueness("plmijnbuhgvytfcrdxesz") is True
    assert passwords.check_uniqueness("65gf") is False


def test_calculate_length_returns_an_integer_length_value():
    assert passwords.calculate_length("password123!") == 12
    assert passwords.calculate_length("letMe1n*") == 8
    assert passwords.calculate_length("$qwerty1234567890") == 17


def test_find_character_type_strength_returns_type_count():
    assert passwords.find_character_type_strength("letMe1n*") == 4
    assert passwords.find_character_type_strength("password123!") == 3
    assert passwords.find_character_type_strength("qw3rty") == 2
    assert passwords.find_character_type_strength("0987654321") == 1


def test_determine_strength_returns_strength():
    assert passwords.determine_strength(12, 4) == "very strong"
    assert passwords.determine_strength(15, 3) == "strong"
    assert passwords.determine_strength(20, 2) == "weak"
    assert passwords.determine_strength(25, 1) == "very weak"
    assert passwords.determine_strength(5, 0) == "very weak"


def test_check_dictionary_returns_correct_Boolean():
    assert passwords.check_dictionary("20-point") is True
    assert passwords.check_dictionary("fnq56") is False


def test_check_common_passwords_returns_correct_Boolean():
    assert passwords.check_common_passwords("air-bound") is True
    # REGEX PATTERN DOESN'T WORK:
    # assert passwords.check_common_passwords("pf8") is False
