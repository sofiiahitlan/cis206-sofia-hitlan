"""This program calculates Body Mass Index(BMI) and provides BMI ranges
for user to determine if his/her BMI is in healthy range.

Input:
    Weight in pounds
    Height in feet and inches


Output:
    BMI
    BMI legend

Example:
    Please enter your weight in pounds:
    125

    Please enter your height in feet
    (remaining inches will be entered seperately):
    5

    Please enter remaining inches:
    6

    Your BMI is 20.2.

    Please refer to the legend to determine if your BMI isin healthy range.

    Below 18.5 - Underweight.
    18.5 to 24.9 - Normal.
    25.0 to 29.9 - Overweight.
    30.0 and Above - Obese.

References:
    * Formula: https://www.cdc.gov/healthyweight/assessing/bmi/
      childrens_bmi/childrens_bmi_formula.html
    * BMI ranges: https://www.bcbst.com/providers/MPMTools/
      BMICalculator.shtm
    * Decimal places: https://www.programiz.com/python-programming/methods/
      built-in/round
    * Functions syntax: https://bitbucket.org/sofiiahitlan/
      cis106-sofiia-hitlan/src/master/Final%20Project/finalproject.py
    * Python formatter:  http://pep8online.com/

"""


def display_welcome():
    """Welcomes user to the program.

    Args: None

    Returns: None

    """
    print("Welcome to the Adult Body Mass Index Calculator! \n")


def get_pounds():
    """Gets weight in pounds.

    Args: None

    Returns:
        int: weight in pounds

    """
    print("Please enter your weight in pounds:")
    weight_in_pounds = int(input())

    return weight_in_pounds


def get_height_feet():
    """Gets height in feet(whole number only).

    Args: None

    Returns:
        int: height in feet (whole number only)

    """
    print("\nPlease enter your height in feet " +
          "(remaining inches will be entered seperately):")
    height_feet = int(input())

    return height_feet


def get_height_inches():
    """Gets the remaining inches of the height.

    Args: None

    Returns:
        int: inches

    """
    print("\nPlease enter remaining inches:")
    height_inches = int(input())

    return height_inches


def convert_height(height_feet, height_inches):
    """Converts height from feet and inches to inches only.

    Args:
        feet (int): feet to be converted
        inches (int): inches to be converted

    Returns:
        int: height in inches

    """
    height_in_inches = height_feet * 12 + height_inches

    return height_in_inches


def calculate_bmi(weight_in_pounds, height_in_inches):
    """Calculates BMI.

    Args:
        weight (int)
        height (int)

    Returns:
        int: BMI

    """
    bmi = 703 * weight_in_pounds / (height_in_inches * height_in_inches)

    return bmi


def display_bmi(bmi):
    """Displays BMI.

    Args: bmi (int)

    Returns: None

    """
    print("\nYour BMI is " + str(round(bmi, 1)) + ".")


def display_bmi_range():
    """Displays BMI legend.

    Args: None

    Returns: None

    """
    print("\nPlease refer to the legend to determine if your BMI is" +
          "in healthy range.\n")
    print("Below 18.5 - Underweight. \n18.5 to 24.9 - Normal. " +
          "\n25.0 to 29.9 - Overweight. \n30.0 and Above - Obese.")


def main():
    """Runs the main program logic."""

    weight_in_pounds = get_pounds()
    height_feet = get_height_feet()
    height_inches = get_height_inches()
    height_in_inches = convert_height(height_feet, height_inches)
    bmi = calculate_bmi(weight_in_pounds, height_in_inches)
    display_bmi(bmi)
    display_bmi_range()


main()
