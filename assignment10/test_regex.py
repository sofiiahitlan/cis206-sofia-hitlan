"""This file tests the regex program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    2 passed in 0.xx seconds
"""
import pytest
import re
import os
import gzip
import regex


file_extension = re.compile(r'(./*.gz)')
files_array = []

for path, _, files in os.walk(os.getcwd()):
    for file in files:
        matches = file_extension.finditer(file)
        for _ in matches:
            file_path = os.path.join(path, file)
            files_array.append(file_path)

def test_get_files_returns_array():
    assert type(regex.get_files()) is list


def test_get_files_returns_correct_array():
    assert regex.get_files() == files_array


# def test_get_records_returns_pages(capsys):
#     pages = {'Adobe_Flash': 1, 'Adobe_Photoshop': 1, 'Advanced_ANOVA/MANOVA': 1}
#     regex.get_records(['test_gz.gz'])

#     captured = capsys.readouterr()
#     assert captured.out == pages


# def test_create_projects_with_subpages_returns_correct_dictionary(capsys):
#     pages = {'Adobe_Flash': 1, 'Adobe_Photoshop': 1, 'Advanced_ANOVA/MANOVA': 1}
#     regex.create_projects_no_subpages(pages)
#     captured = capsys.readouterr()
#     assert captured.out == {'Advanced_ANOVA/MANOVA': 1}
