import os
import re


try:
    file_extension =  re.compile(r'(./*.gz)')
    files_array = []
    for path, directories, files in os.walk(os.getcwd()):
        for file in files:
            matches = file_extension.finditer(file)
            for match in matches:
                file_path = os.path.join(path, file)
                #print(os.path.join(path, file))
                files_array.append(file_path)
                #print(f"File: {os.path.join(path, file)}")
        print(files_array)
except FileNotFoundError:
    print("File was not found.")
    os._exit(1)



#def get_data():
#    """ Reads the file found in the directory.
#
#    Args: none
#
#    Returns: wikiversity_records(dict)
#    """
#    log_file_pattern = re.compile(r'(en\.v)\s([^\s]+)\s(\d+)\s(\d+)')
#
#    try:
#        for file in glob.glob("./*.gz"):
#            print(file)
#            with gzip.open(file, 'r') as file:
#                wikiversity_records = {}
#                for line in file:
#                    line = line.strip()
#                    record = str(line, encoding="utf-8")
#                    matches = log_file_pattern.finditer(record)
#
#                    for match in matches:
#                        wikiversity_records[match.group(2)
#                                            ] = int(match.group(3))
#
#                print("WIKIVERSITY RECORDS", wikiversity_records)
#
#            return wikiversity_records
#    except FileNotFoundError:
#        print("File was not found.")
#        os._exit(1)
