"""This program finds all .gz Wiki hourly log data files in the parent
directory, decodes them, creates a dictionaries of all
en.Wikiversity pages and projects. Each dictionary has a total views
info for each page/project. Program displays top 100 Wikiversity
pages and projects.

Input:
    none

Output:
    wikiversity_pages (top 100, sorted in desc order)
    projects (top 100, sorted in desc order)

Example:
    TOP 100 WIKIVERSITY PAGES:
    Wikiversity:Main_Page: 166
    Special:Search: 48
    Social_problems: 42
    10_Principles_of_Economics: 14
    ...

    TOP 100 WIKIVERSITY PROJECTS:
    Wireshark: 45
    Python_Concepts: 34
    Computer_Networks: 28
    IT_Fundamentals: 20
    Electric_Circuit_Analysis: 19
    ...

References:
    * Gzip library:
        https://docs.python.org/3/library/gzip.html
    * Open all .txt files in the directory:
        https://stackoverflow.com/questions/3964681/find-all-files-in-a
        -directory-with-extension-txt-in-python
    * walk() method:
        https://en.wikiversity.org/wiki/Python_Programming/Files#
        The_os.walk()_Method
    * Sorting dictionary by value using itemgetter:
        1) https://docs.python.org/3/howto/sorting.html#operator-module
           -functions
        2) https://stackoverflow.com/questions/613183/how-do-i-sort-a-
           dictionary-by-value
    * Merge two dictionaries and add values of common keys:
        https://thispointer.com/how-to-merge-two-or-more-dictionaries
        -in-python/
"""
import glob
import gzip
import re
import os
from operator import itemgetter
import sys


def get_files():
    """Finds all .gz files in the parent directory and puts them
    into array.

    Args: none

    Returns: files_array
    """
    try:
        file_extension = re.compile(r'(./*.gz)')
        files_array = []

        for path, _, files in os.walk(os.getcwd()):
            for file in files:
                matches = file_extension.finditer(file)
                for _ in matches:
                    file_path = os.path.join(path, file)
                    files_array.append(file_path)
            if len(files_array) == 0:
                print("No gzip files found. Bye.")
                os._exit(1)
            else:
                return files_array

    except:
        print("Unexpected error.")
        print(f"Error: {sys.exc_info()[1]}")
        print(f"File: {sys.exc_info()[2].tb_frame.f_code.co_filename}")
        print(f"Line: {sys.exc_info()[2].tb_lineno}")


def get_records(files_array):
    """Decodes files in the array, finds all en.wikiversity pages,
    and puts them into dictionary.

    Args: files_array

    Returns: wikiversity_pages(dict)
    """
    log_file_pattern = re.compile(r'(en\.v)\s([^\s]+)\s(\d+)\s(\d+)')

    try:
        wikiversity_pages = {}
        for f in files_array:
            with gzip.open(f, 'r') as file:
                for line in file:
                    line = line.strip()
                    record = str(line, encoding="utf-8")
                    matches = log_file_pattern.finditer(record)

                    for match in matches:
                        if wikiversity_pages.get(match.group(2)) is None:
                            wikiversity_pages[match.group(2)
                                              ] = int(match.group(3))
                        else:
                            wikiversity_pages[match.group(2)
                                              ] += int(match.group(3))
        # print("WIKIVERSITY PAGES", wikiversity_pages)

        return wikiversity_pages
    except FileNotFoundError:
        print("No .gz file was not found.")
        os._exit(1)


def create_projects_with_subpages(wikiversity_pages):
    """Creates a dictionary of learning projects that HAVE subpages.

    Args: wikiversity_pages(dict)

    Returns: subpage_projects(dict)
    """
    subpage_pattern = re.compile(r'(\w+)\/\w+')
    subpage_projects = {}

    for key in wikiversity_pages:
        matches = subpage_pattern.finditer(key)
        for match in matches:
            if subpage_projects.get(match.group(1)) is None:
                subpage_projects[match.group(1)] = wikiversity_pages[key]
            else:
                subpage_projects[match.group(1)] += wikiversity_pages[key]
    # print("PROJECTS WITH SUBFOLDER:", subpage_project)

    return subpage_projects


def create_projects_no_subpages(wikiversity_pages):
    """Creates a dictionary of learning projects that DON'T have subpages.

    Args: wikiversity_pages(dict)

    Returns: no_subpage_projects(dict)
    """
    no_subpage_pattern = re.compile(r'\'(\w+)\'')
    no_subpage_projects = {}
    for key in wikiversity_pages:
        new_key = "'" + key + "'"
        matches = no_subpage_pattern.finditer(new_key)
        for match in matches:
            if no_subpage_projects.get(match.group(1)) is None:
                no_subpage_projects[match.group(1)] = wikiversity_pages[key]
            else:
                no_subpage_projects[match.group(1)] += wikiversity_pages[key]
    # print("PROJECTS WITH NO SUBFOLDER", no_subpage_projects)

    return no_subpage_projects


def merge_projects(subpage_projects, no_subpage_projects):
    """Merges two dictionaries and adds values of common keys.

    Args:
        subpage_projects(dict)
        no_subpage_projects(dict)

    Returns:
        projects(dict)
    """
    projects = {**subpage_projects, **no_subpage_projects}
    for key in projects:
        if key in subpage_projects and key in no_subpage_projects:
            projects[key] = subpage_projects[key] + no_subpage_projects[key]

    return projects


def sort_data(records, label):
    """ Sorts wikiversity pages or projects in descending order based
    on the number of visits. Calls display_results() function.

    Args:
        wikiversity_pages (dict)
        OR
        projects (dict)
        AND
        label(literal string)

    Returns: none
    """
    try:
        if type(records) is not dict:
            raise AttributeError

        records = sorted(records.items(), key=itemgetter(1), reverse=True)

        top100 = {}
        record_count = 0

        for key, value in records:
            top100[key] = value
            record_count += 1
            if record_count > 100:
                break

        display_results(top100, label)

    except AttributeError:
        print("AttributeError: dictionary not of class type dict.")
        print("Received '%s' of type %s." % (records, type(records)))


def display_results(top100, label):
    """ Displays top 100 wikiversity pages or projects.

    Args:
        wikiversity_pages (dict)
        OR
        projects (dict)
        AND
        label(literal string)

    Returns: none
    """
    print(f"\n\nTOP 100 {label}:\n")
    for key in top100:
        print(key + ":", top100[key])


def main():
    """ Runs the main program logic. """
    try:
        files_array = get_files()
        wikiversity_pages = get_records(files_array)
        sort_data(wikiversity_pages, "WIKIVERSITY PAGES")
        subpage_projects = create_projects_with_subpages(wikiversity_pages)
        no_subpage_projects = create_projects_no_subpages(wikiversity_pages)
        projects = merge_projects(subpage_projects, no_subpage_projects)
        sort_data(projects, "WIKIVERSITY PROJECTS")
    except:
        print("Unexpected error.")
        print(f"Error: {sys.exc_info()[1]}")
        print(f"File: {sys.exc_info()[2].tb_frame.f_code.co_filename}")
        print(f"Line: {sys.exc_info()[2].tb_lineno}")


if __name__ == "__main__":
    main()
