"""This program connects to the Northwind database. User can pick one of
the database tables to view its records. Records, column headings and
row numbers are then displayed in an appropriately sized columns.

Input:
    table_choice (int)

Output:
    column headings
    row numbers
    records

Example:
    Enter a number of the table you wish to review records from or
    press Enter to quit.

    1. Regions
    2. Territories
    3. Suppliers
    4. Categories
    ....

    1

    'Regions' table records:

    Row No. RegionID RegionDescription
    1       1        Eastern
    2       2        Western
    3       3        Northern
    4       4        Southern

    Would you like (I)nsert, (U)pdate, or (D)elete a record? You may
    press Enter to quit now.
    inSert, Please

    Enter RegionID to insert:
    6

    Enter RegionDescription to insert:
    SW

    The following record has been inserted: [4, 'SE']

References:
    * How not to select tables that start with 'sqlite':
        https://www.sqlitetutorial.net/sqlite-tutorial/sqlite-show-
        tables/
    *  string.ljust() idea:
        https://stackoverflow.com/questions/9535954/printing-lists-as-
        tabular-data
    * string.ljust() - align left - method. Definition and usage:
        https://www.w3schools.com/python/ref_string_ljust.asp

"""
import sqlite3
import sys
import os


def connect_to_database(database):
    """Creates connection to the Northwind database. Raises exception
    if can't connect.

    Args: database (str, file name)

    Returns: connection (object)

    """
    try:
        connection = sqlite3.connect(database)
    except:
        print("Unable to connect to %s" % database)
        raise Exception
    return connection


def create_cursor(connection):
    """Creates database cursor.

    Args: connection (object)

    Returns: cursor (object)

    """
    cursor = connection.cursor()
    return cursor


def close_connection(connection):
    """Closes database connection.

    Args: connection (object)

    Returns: none

    """
    try:
        connection.close()
        return True
    except:
        print("Couldn't close the database connection.")
        return False


def create_list_of_table_tuples(cursor, connection):
    """Selects table names and creates list of tuples from them.

    Args:
        cursor (object),
        connection (object)

    Returns:
        list_of_table_tuples (array)

    """
    cursor.execute("""SELECT name FROM sqlite_master
                      WHERE type='table' AND name NOT LIKE 'sqlite_%';""")
    list_of_table_tuples = cursor.fetchall()
    return list_of_table_tuples


def create_list_of_tables(list_of_table_tuples):
    """Selects database table names from the list of tuples and puts
    them into an array.

    Args:
        list_of_table_tuples (array)

    Returns:
        tables (array)

    """

    tables = []

    for table in list_of_table_tuples:
        table = table[0]
        tables.append(table)
    return tables


def get_table_choice(tables):
    """Asks the user to select the table to review the records from.
    Raises an error when input is not an integer. Prints error message
    if user picks number of the table that doesn't exist. Kills the
    program if the user wants to quit.

    Args: tables (list)

    Returns:
        table_choice (int)

    """
    while True:
        print("\nEnter a number of the table you wish to review records " +
              "from or press Enter to quit.\n")
        i = 1
        for table in tables:
            print(f"{i}. {table}")
            i += 1
        print()
        table_choice = input()
        if table_choice == "":
            print("Bye.")
            os._exit(1)

        try:
            table_choice = int(table_choice)
            if table_choice not in range(1, len(tables)+1):
                print("No table with such number.")
            else:
                return table_choice

        except ValueError:
            print("Table number must be an integer.")
            print(f"ValueError: {table_choice} is invalid.")
            print(f"Press any key to correct your answer or press enter to " +
                   "quit now.")

            while True:
                stop_choice = input()
                if stop_choice == "":
                    print("Bye.")
                    os._exit(1)
                else:
                    break


def determine_table_name(table_choice, tables):
    """Determines table name based on user's input.

    Args:
        table_choice (int)
        tables (list)

    Returns:
        table_name (str)

    """
    table_name = tables[table_choice - 1]
    return table_name


def determine_table_columns(cursor, table_name):
    """Determines table fields(columns) within a chosen table.

    Args:
        cursor (object)
        table_name (str)

    Returns:
        fields_tuple (tuple of tuples)

    """
    cursor.execute(f"SELECT * FROM {table_name};")
    fields_tuple = cursor.description
    return fields_tuple


def build_fields_array(fields_tuple):
    """Builds table fields array.

    Args: fields_tuple (tuple of tuples)

    Returns: fields (array)

    """
    fields = []
    for field in fields_tuple:
        field = field[0]
        fields.append(field)
    # print("FIELDS", fields)
    return fields


def determine_column_name_size(field):
    """Determines table column(field) size.

    Args: fields (array)

    Returns: column_size (int)

    """
    column_name_size = len(field)
    return column_name_size


def find_table_records(table_name, tables, cursor):
    """Finds all records within a chosen table.

    Args:
        table_name (str),
        tables (list),
        cursor (object)

    Returns:
        records (list of tuples)

    """
    cursor.execute(f"SELECT * FROM {table_name};")
    records = cursor.fetchall()
    return records


def determine_max_record_length(cursor, field, table_name, records):
    """Finds the length of the longest record in a table.

    Args:
        cursor (object)
        field (str, field in fields)
        table_name (str)
        records (list of tuples)

    Returns:
        longest_field_size (int)

    """
    cursor.execute(f"SELECT MAX(LENGTH({field})) FROM {table_name};")
    longest_field_size = cursor.fetchall()
    longest_field_size = longest_field_size[0][0]
    return longest_field_size


def create_sizes_array(cursor, table_name, records, fields):
    """Calls determine_column_name_size() and
    determine_max_record_length() functions for each field.
    Compares field name size and the size of the longest record within
    that field. Makes the bigger number the column size. Appends column
    size to the field_sizes array.

    Args:
        cursor (object),
        table_name (str),
        records (list of tuples)
        fields (array)

    Returns:
       field_sizes (array)

    """
    field_sizes = []

    row_heading_size = determine_column_name_size("Row No.")
    row_count = len(records)
    row_count_size = len(str(row_count))
    if row_heading_size >= row_count_size:
        column_size = row_heading_size
    else:
        column_size = row_count_size

    field_sizes.append(column_size)

    for field in fields:
        column_name_size = determine_column_name_size(field)
        longest_field_size = determine_max_record_length(cursor, field,
                                                         table_name,
                                                         records)
        if column_name_size >= longest_field_size:
            column_size = column_name_size
        else:
            column_size = longest_field_size

        field_sizes.append(column_size)
    return field_sizes


def display_table_records(table_name, fields, records, field_sizes):
    """Displays records of a chosen table in an appropriately sized
    columns. Displays column headings and row numbers.

    Args:
        table_name (str)
        fields (array)
        records (list of tuples)
        field_sizes (array)

    Returns:
        none

    """
    print(f"\n'{table_name}' table records:\n")

    print("Row No.".ljust(field_sizes[0]), end=" ")

    for j in range(0, len(fields)):
        fields[j] = str(fields[j])
        fields[j] = fields[j].ljust(field_sizes[j+1])
        print(fields[j], end=" ")
    print()
    row_number = 1
    for record in records:
        print(str(row_number).ljust(field_sizes[0]), end=" ")
        row_number += 1

        for i in range(0, len(record)):
            element = str(record[i])
            element = element.ljust(field_sizes[i+1])
            print(element, end=" ")
        print()


def get_modify_choice():
    """Offers a user to choose between inserting, updating, and
    deleting a record or exiting a program. Uses string methods to
    process the modify choice by lowercasing it and shortening it to
    one character. Will run until the choice is either "i", "u" or "d".

    Args: none

    Returns: modify_choice (str, one character long)

    """
    while True:
        print("\nWould you like (I)nsert, (U)pdate, or (D)elete a record? " +
              "You may press Enter to quit now.")
        modify_choice = input()

        if modify_choice == "":
            print("Bye.")
            os._exit(1)

        modify_choice = modify_choice[0].lower()
        if modify_choice in "iud":
            break

    return modify_choice


def update_record(fields, cursor, table_name, field_choice, updated_value,
                  record_id, connection):
    """ Asks the user to enter a field name to be updated. Checks if
    such field exists in a table. If no, asks again until the valid
    field has been entered. Then requests a new value for a field.
    Updates the record. Commits the change to the database.

    Args:
        fields (array)
        cursor (object)
        table_name (str)
        record_id (str)
        connection (object)

    Returns:
        none

    """
    # cursor.execute(f"""SELECT {field_choice} from {table_name};""")
    # previous_value = cursor.fetchall()
    # previous_value =  previous_value[int(record_id)-1][0]
    # previous_value_datatype = type(previous_value)
    # print("Previous value:", previous_value_datatype)

    # print("Testing update statement:", table_name, field_choice,
    #       updated_value, fields[0], record_id)

    cursor.execute(f"""UPDATE {table_name}
                       SET {fields[field_choice]} = '{updated_value}'
                       WHERE {fields[0]} = {record_id};""")
    connection.commit()

    # TESTING  CODE BELOW:
    cursor.execute(f"SELECT * FROM {table_name};")
    new_records = cursor.fetchall()
    print("Records after the update:", new_records)


def get_field_to_update(fields):
    print("\nEnter a number of the field to update:")
    while True:
        for i in range(len(fields)):
            print(i+1, "-", fields[i])
        field_choice = input()
        try:
            field_choice = int(field_choice)
            field_choice = field_choice - 1
            break
        except ValueError:
            print("\nField number must be an integer.")
            print(f"ValueError: {field_choice} is invalid. " +
                   "Please, try again.\n")

    return field_choice


def get_new_field_value(fields, field_choice):
    print(f"\nEnter a new value for a {fields[field_choice]} field:")
    updated_value = input()
    updated_value = updated_value.strip()
    return updated_value


def get_record_to_insert(fields):
    """Asks user for a value for each field and adds it to an array.

    Args: fields (array)

    Returns: record_to_insert (array)

    """
    record_to_insert = []
    for field in fields:
        print(f"\nEnter {field} to insert:")
        item_to_insert = input()
        try:
            item_to_insert = int(item_to_insert)
            record_to_insert.append(item_to_insert)
        except:
            record_to_insert.append(item_to_insert)
    print("\nThe following record has been inserted:",
          record_to_insert)

    return record_to_insert


def insert_record(cursor, table_name, fields, record_to_insert, connection):
    """Converts fields array and record_to_insert array into a string
    and strips "[" and "]". Inserts a record into a database and
    commits a change.

    Args:
        cursor (object)
        table_name (str)
        fields (array)
        record_to_insert (array)
        connection (object)

    Returns:
        none

    """
    fields = str(fields)
    fields = fields[1:len(fields)-1]

    record_to_insert = str(record_to_insert)
    record_to_insert = record_to_insert[1:len(record_to_insert)-1]

    cursor.execute(f"""INSERT INTO {table_name}({fields})
                       VALUES({record_to_insert});""")
    connection.commit()

    # TESTING CODE BELOW:
    cursor.execute(f"SELECT * FROM {table_name};")
    new_records = cursor.fetchall()
    print("Records after insert:", new_records)


def get_record_id(process):
    """Asks for an ID of a record a user wants to update/delete. User
    has an option to quit.

    Args: process (literal string)

    Returns: record_id (str)

    """
    print(f"\nEnter an ID for a record you want to {process} or press " +
           "Enter to quit now:")
    record_id = input()
    if record_id == "":
        print("Bye.")
        os._exit(1)

    return record_id


def delete_record(cursor, table_name, fields, record_id, connection):
    """Deletes a record choses by the user.

    Args:
        cursor (object)
        table_name (str)
        fields (array)
        record_id (str)
        connection (object)

    Returns:
        none

    """
    cursor.execute(f"""DELETE FROM {table_name}
                       WHERE {fields[0]} = {record_id};""")
    connection.commit()

    # TEST CODE:
    # cursor.execute(f"SELECT * FROM {table_name};")
    # new_records = cursor.fetchall()
    # print("Records after delete:", new_records)

    print(f"\nRecord with {fields[0]} of {record_id} has been deleted from " +
          f"{table_name} table.")


def modify_again():
    print("\nWould you like to modify a table again? Press any key to " +
          "continue or press Enter to quit now.")

    try_again = input()
    if try_again == "":
        print("Bye.")
        os._exit(1)


def main():
    """ Runs the main program logic. """

    try:
        database = "Northwind.db"
        connection = connect_to_database(database)
        cursor = create_cursor(connection)
        list_of_table_tuples = create_list_of_table_tuples(cursor, connection)
        tables = create_list_of_tables(list_of_table_tuples)
        close_connection(connection)

        table_choice = get_table_choice(tables)
        connection = connect_to_database(database)
        cursor = create_cursor(connection)
        table_name = determine_table_name(table_choice, tables)
        fields_tuple = determine_table_columns(cursor, table_name)
        fields = build_fields_array(fields_tuple)

        records = find_table_records(table_name, tables, cursor)
        field_sizes = create_sizes_array(cursor, table_name, records, fields)
        display_table_records(table_name, fields, records, field_sizes)

        while True:
            modify_choice = get_modify_choice()

            if modify_choice == "i":
                record_to_insert = get_record_to_insert(fields)
                insert_record(cursor, table_name, fields, record_to_insert,
                              connection)
            elif modify_choice == "u":
                record_id = get_record_id("update")
                field_choice = get_field_to_update(fields)
                updated_value = get_new_field_value(fields, field_choice)
                update_record(fields, cursor, table_name, field_choice,
                              updated_value, record_id, connection)
            elif modify_choice == "d":
                record_id = get_record_id("delete")
                delete_record(cursor, table_name, fields, record_id,
                              connection)
            else:
                print("Unexpected error.")

            modify_again()

        close_connection(connection)
    except:
        print("Unexpected error.")
        print(f"Error: {sys.exc_info()[1]}")
        print(f"File: {sys.exc_info()[2].tb_frame.f_code.co_filename}")
        print(f"Line: {sys.exc_info()[2].tb_lineno}")
    finally:
        close_connection(connection)


if __name__ == "__main__":
    main()
