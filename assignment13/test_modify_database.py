"""This file tests database program using PyTest.

Run "pytest" in this folder to automatically run these tests.

Expected output:
    x passed in 0.xx seconds
"""
import pytest
import re
import sqlite3
import modify_database


connection = sqlite3.connect("Northwind.db")
cursor = connection.cursor()

list_of_table_tuples = [('Regions',), ('Territories',), ('Suppliers',),
                        ('Categories',), ('Products',), ('Employees',),
                        ('Customers',), ('Orders',), ('OrderDetails',),
                        ('InternationalOrders',),
                        ('EmployeesTerritories',)]

tables = ['Regions', 'Territories', 'Suppliers', 'Categories',
          'Products', 'Employees', 'Customers', 'Orders', 'OrderDetails',
          'InternationalOrders', 'EmployeesTerritories']

regions_records = [(1, 'Eastern'), (2, 'Western'), (3, 'Northern'),
                   (4, 'Southern')]

region_tuple = (('RegionID', None, None, None, None, None, None),
                ('RegionDescription', None, None, None, None, None, None))

sizes_array = [7, 8, 17]


def test_connect_to_database_connects_successfuly():
    modify_database.connect_to_database("test.db")


# def test_connect_to_database_raises_when_fails():
#     with pytest.raises(Exception):
#         modify_database.connect_to_database(a)


def test_create_cursor_creates_cursor():
    cursor = modify_database.create_cursor(connection)
    hasattr(cursor, 'description')


def test_create_list_of_table_tuples_returns_correct_list():
    assert modify_database.create_list_of_table_tuples(cursor, connection) == list_of_table_tuples


def test_create_list_of_tables_returns_correct_list():
    assert modify_database.create_list_of_tables(list_of_table_tuples) == tables


def test_get_table_choice_returns_correct_integer():
    input_values = [1]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input
    assert modify_database.get_table_choice(tables) == 1


def test_determine_table_name_returns_correct_name():
    assert modify_database.determine_table_name(1, tables) == 'Regions'
    assert modify_database.determine_table_name(2, tables) == 'Territories'
    assert modify_database.determine_table_name(9, tables) == 'OrderDetails'


def test_determine_table_columns_returns_correct_tuple():
    assert modify_database.determine_table_columns(cursor,
                                                   'Regions') == region_tuple


def test_build_fields_array_returns_correct_list():
    assert modify_database.build_fields_array(region_tuple) == ['RegionID', 'RegionDescription']


def test_determine_column_name_size_returns_correct_size():
    assert modify_database.determine_column_name_size("LastName") == 8
    assert modify_database.determine_column_name_size("No.") == 3
    assert modify_database.determine_column_name_size("Phone_Number") == 12


def test_find_table_records_returns_correct_list_of_record_tuples():
    assert modify_database.find_table_records("Regions", tables, cursor) == regions_records


def test_determine_max_record_length_returns_correct_length():
    assert modify_database.determine_max_record_length(cursor, "RegionID",
                                                       "Regions",
                                                       regions_records) == 1


def test_create_sizes_array_returns_correct_array():
    assert modify_database.create_sizes_array(cursor, "Regions", regions_records,
                                              ["RegionID", "RegionDescription"]) == sizes_array


def test_display_table_records_display_output_correctly():
    modify_database.display_table_records("Regions",
                                          ["RegionID", "RegionDescription"],
                                          regions_records, sizes_array) == """
                                          'Regions' table records:

                                          Row No. RegionID RegionDescription
                                          1       1        Eastern
                                          2       2        Western
                                          3       3        Northern
                                          4       4        Southern"""


def test_get_modify_choice_returns_correct_char():
    input_values = ["delete DATABASE"]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input
    assert modify_database.get_modify_choice() == "d"


def test_get_record_to_insert_ouputs_correct_inserted_record(capsys):

    input_values = ["Test", 9]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input
    assert modify_database.get_record_to_insert(['RegionID',
                                                 'RegionDescription'
                                                 ]) == [9, "Test"]

    captured = capsys.readouterr()
    assert captured.out == ("\nEnter RegionID to insert:\n\nEnter " +
                            "RegionDescription to insert:\n\nThe following " +
                            "record has been inserted: [9, 'Test']\n")


def test_get_record_id_returnds_correct_id():
    input_values = [1]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input
    assert modify_database.get_record_id("update") == 1


def test_delete_record_deletes_a_record_successfully():
    cursor.execute(f"SELECT * FROM Regions;")
    old_records = cursor.fetchall()

    cursor.execute(f"""INSERT INTO Regions ('RegionID', 'RegionDescription')
                       VALUES(50, 'Test Delete');""")
    connection.commit()

    modify_database.delete_record(cursor, "Regions", ['RegionID',
                                  'RegionDescription'], 50, connection)

    cursor.execute(f"SELECT * FROM Regions;")
    new_records = cursor.fetchall()
    new_records == old_records


def test_get_field_to_update_returns_correct_index():
    input_values = [1]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input

    assert modify_database.get_field_to_update(['RegionID', 'RegionDescription']) == 0


def test_get_new_field_value_returns_correct_value():
    input_values = [" Test "]

    def input(prompt=None):
        return input_values.pop()

    modify_database.input = input

    assert modify_database.get_new_field_value(['RegionID', 'RegionDescription'], 0) == "Test"


def test_update_record_updates_successfully():
    modify_database.insert_record(cursor, "Regions", ['RegionID',
                                  'RegionDescription'], [9, "Test Update"],
                                  connection)
    connection.commit()

    modify_database.input = input
    modify_database.update_record(['RegionID',
                                   'RegionDescription'],
                                  cursor, "Regions", 1, "Test 2", 85, connection)

    connection.commit()

    cursor.execute(f"""DELETE FROM Regions
                       WHERE RegionID = 9;""")
    connection.commit()


def test_insert_record_inserts_record_successfully():
    cursor.execute(f"SELECT * FROM Regions;")
    old_records = cursor.fetchall()

    modify_database.insert_record(cursor, "Regions", ['RegionID',
                                  'RegionDescription'], [70, "Test insert"],
                                  connection)

    cursor.execute(f"SELECT * FROM Regions;")
    new_records = cursor.fetchall()

    new_records == old_records + [(70, "Test insert")]

    cursor.execute(f"""DELETE FROM Regions
                       WHERE RegionID = 70;""")
    connection.commit()


def test_close_connection_closes_connection():
    assert modify_database.close_connection(connection) is True


def test_close_connection_returns_False_if_can_not_close_connection():
    assert modify_database.close_connection("") is False
